package mstparser;

import java.io.Serializable;

/**
 * Created by hillel on 10/2/14.
 */
public class ArcPart implements Part, Comparable, Serializable {
    private static final long serialVersionUID = 1L;
    public int h;
    public int m;

    public ArcPart(int h, int m) {
        this.h = h;
        this.m = m;
    }

    public ArcPart(ArcPart o) {
        this(o.h, o.m);
    }



    @Override
    public PartType type() {
        return PartType.ARC_PART;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArcPart arcPart = (ArcPart) o;

        if (h != arcPart.h) return false;
        if (m != arcPart.m) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = h;
        result = 31 * result + m;
        return result;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof ArcPart))
            throw new ClassCastException("An ArcPart object expected.");
        int mDiff = this.m - ((ArcPart)o).m;
        return mDiff != 0 ? mDiff : this.h - ((ArcPart)o).h;

    }
}

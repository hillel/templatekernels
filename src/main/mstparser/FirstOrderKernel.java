package mstparser;

/**
 * Created by hillel on 8/22/14.
 */
public class FirstOrderKernel {


    // parts must be arc parts.
    public double sim(DependencyInstance inst1, ArcPart part1,
                      DependencyInstance inst2, ArcPart part2) {
        int m1 = part1.m;
        int m2 = part2.m;
        int h1 = part1.h;
        assert(h1==inst1.heads[m1]);
        int h2 = part2.h;
        assert(h2==inst2.heads[m2]);

        double identicalHeadFeats = 0;
        double identicalEdgeFeats = 1;
        double identicalModifierFeats = 0;

        for (int i = 0; i < inst1.nodeFeats[h1].length; ++i) {
            long currFeat_1 = inst1.nodeFeats[h1][i];
            long currFeat_2 = inst2.nodeFeats[h2][i];
            if (currFeat_1 != 0 && currFeat_1 == currFeat_2) {
                identicalHeadFeats++;
            }
        }

        if (identicalHeadFeats > 0) {

            for (int i=0; i<inst1.nodeFeats[m1].length; ++i) {
                long currFeat_1 = inst1.nodeFeats[m1][i];
                long currFeat_2 = inst2.nodeFeats[m2][i];
                if (currFeat_1 != 0  && currFeat_1 == currFeat_2) {
                    identicalModifierFeats++;
                }
            }

            for (int i=0; i<inst1.edgeFeats[m1].length; ++i) {
                long currFeat_1 = inst1.edgeFeats[m1][i];
                long currFeat_2 = inst2.edgeFeats[m2][i];
                if (currFeat_1 == currFeat_2) {
                    identicalEdgeFeats++;
                }
            }

        }




        return identicalHeadFeats * identicalEdgeFeats * identicalModifierFeats;
    }
}

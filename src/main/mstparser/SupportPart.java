package mstparser;

import mstparser.Part;

import java.io.Serializable;

/**
 * Created by hillel on 10/2/14.
 */
public class SupportPart implements Serializable{
    private static final long serialVersionUID = 1L;
    public Part part;
    public double backupWeight;
    public double weight;
    public double total;
    public int i; // train sentence idx
    public int j; // index into the kbest list for the sentence

    public SupportPart(Part part, double weight, double total, int i, int j) {
        this.part = part;
        this.weight = weight;
        this.total = total;
        this.i = i;
        this.j = j;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SupportPart that = (SupportPart) o;

        if (i != that.i) return false;
        if (j != that.j) return false;
        if (!part.equals(that.part)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = part.hashCode();
        result = 31 * result + i;
        result = 31 * result + j;
        return result;
    }
}

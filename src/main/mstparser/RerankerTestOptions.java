package mstparser;

import java.io.Serializable;

/**
 * Created by hillel on 6/4/14.
 */
public class RerankerTestOptions implements Serializable {
    static final long serialVersionUID = 43L;

    public Integer baseFeatures;
    public Integer kernelOrder;
    public String  lossType;
    public Boolean useBaseScore;
    public Boolean rerankerLabeledLoss;
    public Integer rerankerPruneK;
    public Integer numRerankerIters;
    public Integer maxTreesPerSentence;
    public Integer maxTrainSentences;
    public Double  lambda;


    @Override
    public String toString() {
        return "RerankerTestOptions{" +
                "baseFeatures=" + baseFeatures +
                ", kernelOrder" + kernelOrder +
                ", useBaseScore=" + useBaseScore +
                ", rerankerLabeledLoss=" + rerankerLabeledLoss +
                ", rerankerPruneK=" + rerankerPruneK +
                ", numRerankerIters=" + numRerankerIters +
                ", maxTreesPerSentence=" + maxTreesPerSentence +
                ", maxTrainSentences=" + maxTrainSentences+
                ", lossType=" + lossType+
                ", lambda=" + lambda+
                '}';
    }
}

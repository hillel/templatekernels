package mstparser;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

class MyLogFormatter extends Formatter {


    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder(1000);
        builder.append(record.getMessage());
        return builder.toString();
    }


}

package mstparser;

import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.javatuples.Tuple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;


/**
 * Implementation of a parallelized 1st order template kernel
 */
public class KernelParameters {
    private static final Logger logger = Logger.getLogger(KernelReranker.class.getName());

    public Dataset trainDataset;
    private int kernelOrder;
    Random rand;
    public List<SupportPart> firstOrderSupportParts = new ArrayList<>();
    private FirstOrderKernel firstOrderKernel;
    // Cache from an edge in one of the kBestInstances to the score of multiplying it with
    // all the support vectors. Note that this score will be identical for all the identical
    // edges in the kBest list, so we can cache it.
    private Map<Pair<Integer, Integer>, Double> kBestSingleEdgeCache = new HashMap<>();
    private Map<Triplet<Integer, Integer, Integer>, Double> kBestTwoEdgeCache = new HashMap<>();
    private final ExecutorService exec;
    int threadPoolSize;

    public void ShutdownThreads() {
        exec.shutdown();
    }

    /**
     * A per instance summary of the previous iteration results: the score it got
     * and the support vectors that were used in calculating that score (indicated
     * by an index to the firstOrderSupportParts list that points to one past the last
     * support instance used)
     */
    private class PrevIterationData {

        // The score the instance got in the previous iteration
        public double prevScore;

        // An index that points to one past the last support instance used when
        // calculating the instance score.
        public int firstOrderSupportInstancesEndIdx;

        public PrevIterationData(int lastIncludedFirstOrderSupportIdx, double prevScore) {
            this.prevScore = prevScore;
            this.firstOrderSupportInstancesEndIdx = lastIncludedFirstOrderSupportIdx;
        }
    }

    public void setKernelOrder(int kernelOrder) {
        this.kernelOrder = kernelOrder;
    }

    private Map<Pair<Integer, Integer>, PrevIterationData> prevIterationsData = new HashMap<>();

    private void removeDuplicatesFromSupport() {
        logger.info(String.format("Removing duplicates from support set. initial size=%d%n", firstOrderSupportParts.size()));
        firstOrderSupportParts = removeDuplicateFromSupportList(firstOrderSupportParts);
        logger.info(String.format("Support size after removing duplicates=%d%n", firstOrderSupportParts.size()));
    }

    private List<SupportPart> removeDuplicateFromSupportList(List<SupportPart> support) {
        Map<SupportPart, Double> noDupes1 = new HashMap<>();
        for (SupportPart supportPart : support) {
            if (noDupes1.containsKey(supportPart)) {
                Double prevParam = noDupes1.get(supportPart);
                noDupes1.put(supportPart, prevParam + supportPart.weight);
            } else {
                noDupes1.put(supportPart, supportPart.weight);
            }
        }
        support = new ArrayList();
        for (Map.Entry<SupportPart, Double> entry : noDupes1.entrySet()) {
            SupportPart part = entry.getKey();
            part.weight = entry.getValue();
            support.add(part);
        }
        return support;
    }

    private void clearPrevIterationsData() {
        prevIterationsData.clear();
    }

    /**
     * In test our prevIterationsData cache has to be cleaned, we also
     * remove duplicates from our support set.
     */
    public void prepareForTestPhase() {

        removeDuplicatesFromSupport();
        clearPrevIterationsData();
    }

    public void clearKBestCache() {
        kBestSingleEdgeCache.clear();
        kBestTwoEdgeCache.clear();
    }

    public KernelParameters(int threadPoolSize, int kernelOrder) {

        this.firstOrderKernel = new FirstOrderKernel();
        this.threadPoolSize = threadPoolSize;
        this.kernelOrder = kernelOrder;

        exec = Executors.newFixedThreadPool(threadPoolSize);
        rand = new Random();
        rand.setSeed(123);


    }

    public KernelParameters(Dataset dataset, int threadPoolSize, int kernelOrder) {
        this.trainDataset = dataset;
        this.firstOrderKernel = new FirstOrderKernel();
        this.threadPoolSize = threadPoolSize;
        this.kernelOrder = kernelOrder;
        exec = Executors.newFixedThreadPool(threadPoolSize);
        rand = new Random();
        rand.setSeed(123);
    }

    public void averageParams(double remainingUpdates, double totalSteps) {
        for (SupportPart supportPart : firstOrderSupportParts) {
            supportPart.backupWeight = supportPart.weight;
            supportPart.weight = (supportPart.total - (supportPart.backupWeight * remainingUpdates)) / totalSteps;
        }
    }

    public void unaverageParams() {
        for (SupportPart supportPart : firstOrderSupportParts) {
            supportPart.weight = supportPart.backupWeight;
        }
    }

    public void update(int i, int j, double alpha, double upd) {
        if (kernelOrder < 1) {
            return;
        }

        int oracleIdx = trainDataset.get(i).getOracleInstanceIdx();
        DependencyInstance oracleInstance = trainDataset.get(i).get(oracleIdx);
        DependencyInstance predInstance = trainDataset.get(i).get(j);

        assert(oracleInstance.first_order_siblings.length == predInstance.first_order_siblings.length);
        for (int idx=1; idx<oracleInstance.first_order_siblings.length; ++idx) {
            if (!oracleInstance.first_order_siblings[idx].equals(predInstance.first_order_siblings[idx])) {
                ArcPart oraclePart = oracleInstance.first_order_siblings[idx];
                assert(oraclePart != null);
                firstOrderSupportParts.add(new SupportPart(oraclePart, alpha, alpha*upd, i, oracleIdx));

                ArcPart predPart = predInstance.first_order_siblings[idx];
                assert(predPart != null);
                firstOrderSupportParts.add(new SupportPart(predPart, -1 * alpha, -1 * alpha * upd, i, j));
            }

        }

    }

    public class SumSupport implements Callable<Double>{

        private int startIdx;
        private int endIdx;
        DependencyInstance inst;
        private FirstOrderKernel kernel;
        private Part part;
        /**
         * This has to do with a performance trick:
         * - in training we just add instances to the support list even they are
         *   already present. This allows us to use the {@link #prevIterationsData}
         *   to cache previous calculations. However, it means that each support
         *   instance should always be multiplied by exactly 1, and not by its parameter
         *   value.
         * - in test, the client of the parameter class should call the
         *   {@link #removeDuplicatesFromSupport()} and {@link #clearPrevIterationsData()}
         *   methods (and optionaly the {@link #averageParams(double, double)} method) to
         *   obtain optimal performance.
         */
        private List<SupportPart> support;
        SumSupport(DependencyInstance instance,
                   Part part,
                   List<SupportPart> support,
                   int startIdx,
                   int endIdx,
                   FirstOrderKernel kernel) {
            this.inst = instance;
            this.startIdx = startIdx;
            this.endIdx = endIdx;
            this.support = support;
            this.kernel = kernel;
            this.part = part;
        }

        @Override
        public Double call() throws Exception {
            double perPartSum = 0.0;
            for (int currSupportIdx=startIdx; currSupportIdx<endIdx; ++currSupportIdx) {
                SupportPart currSupport = support.get(currSupportIdx);
                DependencyInstance supportInstance = trainDataset.get(currSupport.i).get(currSupport.j);
                perPartSum += currSupport.weight * kernel.sim(supportInstance, (ArcPart)currSupport.part, inst, (ArcPart)part);
            }
            return perPartSum;
        }
    }

    public double getScoreParallel(Dataset dataset, int sentenceNum, int kBestIdx, boolean useTrainCache) throws ExecutionException, InterruptedException {
        double allPartsSum = 0.0;
        Pair<Integer, Integer> instIdx = Pair.with(sentenceNum, kBestIdx);

        double prevSum = 0.0;
        int lastIncludedFirstOrderSupport = 0;
        PrevIterationData instancePrevData = prevIterationsData.get(instIdx);
        if (useTrainCache && instancePrevData != null) {
            prevSum = instancePrevData.prevScore;
            lastIncludedFirstOrderSupport = instancePrevData.firstOrderSupportInstancesEndIdx;
        }

        allPartsSum+=prevSum;
        DependencyInstance inst = dataset.get(sentenceNum).get(kBestIdx);
        List support = firstOrderSupportParts;
        int remainingSize = support.size() - lastIncludedFirstOrderSupport;


        if (kernelOrder>=1 && remainingSize > 0) {

            for (ArcPart arcPart : inst.first_order_siblings){
                if (arcPart!=null) {
                    Pair<Integer, Integer> edgeIndices = Pair.with(arcPart.h, arcPart.m);
                    double perPartSum = calcPerPartSum(lastIncludedFirstOrderSupport,
                            inst,
                            arcPart,
                            support,
                            remainingSize,
                            edgeIndices,
                            kBestSingleEdgeCache,
                            firstOrderKernel);
                    kBestSingleEdgeCache.put(edgeIndices, perPartSum);
                    allPartsSum += perPartSum;
                }
            }
        }


        if (useTrainCache) {
            prevIterationsData.put(instIdx, new PrevIterationData(firstOrderSupportParts.size(), allPartsSum));
        }
        return allPartsSum;
    }

    private double calcPerPartSum(int lastIncludedSupport,
                                  DependencyInstance instance,
                                  Part part,
                                  List support,
                                  int remainingSize,

                                  Tuple edgeIndices,
                                  Map<? extends Tuple, Double> kbestCache,
                                  FirstOrderKernel kernel) throws InterruptedException, ExecutionException {
        Double perPartSum = 0.0;
        Double score = kbestCache.get(edgeIndices);
        int nTasks = threadPoolSize;
        if (score == null) {
            List<Future<Double>> resultsList = new ArrayList<>();
            for (int t = 0; t < nTasks; ++t) {
                int startIdx = lastIncludedSupport + t*(remainingSize/nTasks);
                int endIdx = support.size();
                if (t<nTasks-1) {
                    endIdx = lastIncludedSupport + (t+1)*(remainingSize/nTasks);
                }

                SumSupport sumSupport = new SumSupport(instance, part, support, startIdx, endIdx, kernel);

                Future<Double> future  = exec.submit(sumSupport);
                resultsList.add(future);

            }
            // exec.shutdown();
            for (Future<Double> result : resultsList) {
                perPartSum += result.get();
            }
        } else {
            perPartSum = score;
        }

        return perPartSum;
    }




    /**
     *
     * @return the size of the support set
     */
    public int getSupportSize(){
        return firstOrderSupportParts.size();
    }

}

package mstparser;

import mstparser.io.RerankerReader;
import sun.rmi.log.LogHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Logger;

/**
 * Created by hillel on 4/9/14.
 */
public class Dataset {
    private static final Logger logger = Logger.getLogger(KernelReranker.class.getName());

    public class Sentence {

        public Sentence(List<DependencyInstance> sentenceTrees) {
            this.sentenceTrees = sentenceTrees;
            this.oracleInstanceIdx = findOracleInstance(sentenceTrees);
        }

        public DependencyInstance get(int i) {
            return sentenceTrees.get(i);
        }

        public int size() {
            return sentenceTrees.size();
        }

        public int getOracleInstanceIdx() {
            return oracleInstanceIdx;
        }

        public DependencyInstance getOracleInstance() {
            return this.sentenceTrees.get(oracleInstanceIdx);
        }

        public List<DependencyInstance> getTrees() {
            return sentenceTrees;
        }
        private List<DependencyInstance> sentenceTrees;
        private int oracleInstanceIdx;


    }

    private List<Sentence> allSentences =new ArrayList<Sentence>();
    private ParserOptions options = null;
    private int totalToks;


    public int getTotalToks() {
        return totalToks;
    }

    // we don't need to save new text strings at each tree, the lower trees can use the strings of gold tree
    private void saveSomeMemory(List<DependencyInstance> trees) {
        DependencyInstance goldTree = trees.get(0);
        goldTree.actParseTree = null;
       // System.out.printf("instance takes %d memory%n", MemoryMeasurer.measureBytes(goldTree));
        /*
        System.out.printf("common parts take %d memory%n", MemoryMeasurer.measureBytes(goldTree.forms)+
                MemoryMeasurer.measureBytes(goldTree.forms) +
                MemoryMeasurer.measureBytes(goldTree.formids) +
                MemoryMeasurer.measureBytes(goldTree.postags) +
                MemoryMeasurer.measureBytes(goldTree.postagids) +
                MemoryMeasurer.measureBytes(goldTree.cpostags) +
                MemoryMeasurer.measureBytes(goldTree.cpostagids) +
                MemoryMeasurer.measureBytes(goldTree.lemmas) +
                MemoryMeasurer.measureBytes(goldTree.lemmaids) +
                MemoryMeasurer.measureBytes(goldTree.feats) +
                MemoryMeasurer.measureBytes(goldTree.featids) +
                MemoryMeasurer.measureBytes(goldTree.isPunct));
        System.out.printf("unique parts take %d memory %n", MemoryMeasurer.measureBytes(goldTree.heads)+
                MemoryMeasurer.measureBytes(goldTree.deps) +
                MemoryMeasurer.measureBytes(goldTree.first_order_siblings) +
                MemoryMeasurer.measureBytes(goldTree.second_order_siblings) +
                MemoryMeasurer.measureBytes(goldTree.st) +
                MemoryMeasurer.measureBytes(goldTree.en) +
                MemoryMeasurer.measureBytes(goldTree.nodeFeats) +
                //MemoryMeasurer.measureBytes(goldTree.nodeFeatValues) +
                MemoryMeasurer.measureBytes(goldTree.edgeFeats) +
                //MemoryMeasurer.measureBytes(goldTree.edgeFeatValues) +
                MemoryMeasurer.measureBytes(goldTree.MSTScore) +

                MemoryMeasurer.measureBytes(goldTree.fv) +
                MemoryMeasurer.measureBytes(goldTree.actParseTree) +
                MemoryMeasurer.measureBytes(goldTree.deprels) +
                MemoryMeasurer.measureBytes(goldTree.deprelids) +
                        MemoryMeasurer.measureBytes(goldTree.relFeats) +
                MemoryMeasurer.measureBytes(goldTree.confidenceScores));
            */
        //ObjectGraphMeasurer.Footprint footprint = ObjectGraphMeasurer.measure(goldTree);
        for (int i=1; i<trees.size(); ++i) {
            DependencyInstance currTree = trees.get(i);
            currTree.forms = goldTree.forms;
            currTree.formids = goldTree.formids;
            currTree.postags = goldTree.postags;
            currTree.postagids = goldTree.postagids;
            currTree.cpostags = goldTree.cpostags;
            currTree.cpostagids = goldTree.cpostagids;
            currTree.lemmas = goldTree.lemmas;
            currTree.lemmaids = goldTree.lemmaids;
            currTree.feats = goldTree.feats;
            currTree.featids = goldTree.featids;
            currTree.isPunct = goldTree.isPunct;
            currTree.actParseTree = null;
        }
    }

    public Dataset() {

    }
    public Dataset(String datafile, ParserOptions options) throws IOException {
        this(datafile, options, null);
    }

    public Dataset(String datafile, ParserOptions options, KernelRerankerPipe pipe) throws IOException {

        this.options = options;
        RerankerReader depReader = new RerankerReader(false);
        depReader.startReading(datafile);
        DependencyInstance instance = depReader.getNext();
        if (instance.feats == null) {
            instance.feats = new String[instance.forms.length][0];
        }
        int instanceCount=0;

        List<DependencyInstance> sentenceTrees = new ArrayList<DependencyInstance>();
        while(instance != null) {


            if (instance.rank == 0 || /* backword compatability to old format */  Util.doubleEquals(instance.MSTScore, -500)) {
                if(sentenceTrees.size() > 0) {
                    if (instanceCount % 1000 == 0) {
                        logger.info(String.format("%d ", instanceCount));
                    }
                    ++instanceCount;
                    saveSomeMemory(sentenceTrees);
                    Sentence sentence = new Sentence(sentenceTrees);
                    allSentences.add(sentence);
                    totalToks+=sentence.get(0).length();
                    sentenceTrees = new ArrayList<>();
                    if (allSentences.size() >= options.maxTrainSentences) {
                        logger.warning("not using all sentences in the dataset due to a low maxTrainSentences!!");
                        break;
                    }
                }

            }


            if (sentenceTrees.size() < options.maxTreesPerSentence + 1) { // +1 since I'm not counting the gold tree
                if (pipe != null) {
                    instance.setInstIds(pipe.getTagDictionary(), pipe.getWordDictionary(), pipe.getTypeAlphabet());
                    pipe.setNodeFeatVals(instance);
                }
                StringBuffer spans = new StringBuffer(instance.heads.length*5);
                for(int i = 1; i < instance.heads.length; i++) {
                    spans.append(instance.heads[i]).append("|").append(i).append(":").append(instance.deplbids[i]).append(" ");
                }
                instance.actParseTree = spans.substring(0, spans.length() - 1);
                sentenceTrees.add(instance);

            }

            instance = depReader.getNext();
            if (instance != null && instance.feats == null) {
                instance.feats = new String[instance.forms.length][0];
            }
        }

        if(sentenceTrees.size() > 0) {
            saveSomeMemory(sentenceTrees);
            Sentence sentence = new Sentence(sentenceTrees);
            allSentences.add(sentence);
            totalToks+=sentence.get(0).length();
        }
        logger.info(String.format("Read %d sentences from the dataset. Read up to %d trees per sentence.\n", allSentences.size(), options.maxTreesPerSentence));
    }

    public int size() {
        return allSentences.size();
    }

    public void addFeaturesToAllSentences(KernelRerankerPipe pipe) {
        for (int i=0; i<this.size(); ++i) {
            if (i%100==0) {
                System.out.println(String.format("Completed adding features for %d sentences", i));
            }
            List<DependencyInstance> sentenceTrees = get(i).getTrees();
            addFeaturesToTrees(sentenceTrees, pipe);
        }
    }

    public void addFeaturesToTrees(Sentence sentence, KernelRerankerPipe pipe) {
        addFeaturesToTrees(sentence.getTrees(), pipe);
    }

    public void addFeaturesToTrees(List<DependencyInstance> instances, KernelRerankerPipe pipe) {
        for (DependencyInstance instance: instances) {
            instance.setFeatureVector(pipe.createFeatureVector(instance));
        }
    }

    public void removeFeaturesFromTrees(Sentence sentence) {
        removeFeaturesFromTrees(sentence.getTrees());
    }

    public void removeFeaturesFromTrees(List<DependencyInstance> instances) {
        for (DependencyInstance instance: instances) {
            instance.setFeatureVector(null);
        }
    }


    public Sentence get(int id) {
        Sentence sentence = allSentences.get(id);
        return sentence;
    }

    public void add(Sentence sentence) {
        allSentences.add(sentence);

    }

    public int findOracleInstance(List<DependencyInstance> instances) {
        DependencyInstance goldInstance = instances.get(0);
        double bestDist=Double.POSITIVE_INFINITY;
        int oracleInstanceIdx = -1;
        for (int i=1; i<instances.size(); ++i){
            DependencyInstance instance = instances.get(i);
            double currDist = goldInstance.dist(instance, options.isNoPuncMode(), options.rerankerLabeledLoss);
            if (currDist < bestDist) {
                bestDist = currDist;
                oracleInstanceIdx = i;

            }
        }
        assert(oracleInstanceIdx!=-1);
        return oracleInstanceIdx;
    }

    public int getOracleInstanceIdx(int sentenceNum) {
        return allSentences.get(sentenceNum).getOracleInstanceIdx();
    }

    public DependencyInstance getOracleInstance(int sentenceNum) {
        Sentence sentence = allSentences.get(sentenceNum);
        return sentence.get(sentence.getOracleInstanceIdx());
    }



}

///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2007 University of Texas at Austin and (C) 2005
// University of Pennsylvania and Copyright (C) 2002, 2003 University
// of Massachusetts Amherst, Department of Computer Science.
//
// This software is licensed under the terms of the Common Public
// License, Version 1.0 or (at your option) any subsequent version.
// 
// The license is approved by the Open Source Initiative, and is
// available from their website at http://www.opensource.org.
///////////////////////////////////////////////////////////////////////////////

package mstparser;

import java.io.File;

/**
 * Hold all the options for the parser so they can be passed around easily.
 *
 * <p>
 * Created: Sat Nov 10 15:25:10 2001
 * </p>
 *
 * @author Jason Baldridge
 * @version $Id: CONLLReader.java 103 2007-01-21 20:26:39Z jasonbaldridge $
 * @see mstparser.io.DependencyReader
 */
public final class ParserOptions {
    
    public String trainfile = null;
    public String testfile = "";
    public boolean trainReranker = false;
    public boolean eval = false;

    public boolean testReranker = false;
    public String rerankerModelName = "";
    public String lossType = "punc";
    public boolean rerankerLabeledLoss= false;
    public boolean dumbReranker = false;
    public String format = "CONLL";
    public String outFormat = "CONLL";
    public boolean tuneReranker = false;
    public int numRerankerIters=15;

    // The following two parameters can be changed when doing feature engineering to reduce training times.
    public int maxTreesPerSentence = 100; // A rerenker param that determines the maximal size of the k-best list (i.e. will not read more than this number of trees from a reranker k-best file).
    public int maxTrainSentences = 100000; // A reranker param that determines the maximal number of training sentences to take into the dataset.
    public int threadPoolSize = 4;

    public String outfile = "out.txt";
    public String validationFile = "";

    public String goldfile = null;
    public double baseScoreParam = 1.0; // If using the MST base score in reranking, we would multiply it by this param.
    public double lambda = 1.0; // The reranker uses this to discount heavily lexicalized features.
    public double learningLambda = 1e-15; // Used by the reranker in PA mode.

    /**
     * 0 - add just the MST score as a reranker feature
     * 1 - add all the features of 2nd order MST to the reranker.
     * 2 - add both 2nd and 3rd order features.
     */
    public int baseFeatures = 0;

    /**
     *  0 - no kernel
     *  1 - first order kernel
     */
    public int kernelOrder = 1;

    /**
     * 0 - prediction based
     * 1 - max-loss
     */
    public int PAType = 0;

    public String rerankerType = "perceptron";

    /**
     * The maximal number of trees (per sentence) from which we extract features.
     */
    public int rerankerPruneK=5;
    public boolean useAveraging = true;
    public boolean useBaseSCore = false;

    public String[] args;



    public ParserOptions (String[] args) {
        this.args = args;
        fromCommandLineArgs(args);
    }

    public  boolean isNoPuncMode(){
        boolean noPunc;
        if (this.lossType.equals("nopunc")){
            noPunc = true;
        } else if (this.lossType.equals("punc")) {
            noPunc = false;
        } else {
            throw new RuntimeException("Invalid parameter value for lossType");
        }
        return noPunc;
    }


    public String toString () {
	StringBuilder sb = new StringBuilder();
	sb.append("FLAGS [");
	sb.append("train-file: " + trainfile);
	sb.append(" | ");
	sb.append("test-file: " + testfile);
	sb.append(" | ");
	sb.append("gold-file: " + goldfile);
	sb.append(" | ");
	sb.append("output-file: " + outfile);
	sb.append(" | ");
    sb.append("validation-file: " + validationFile);
    sb.append(" | ");
    sb.append("reranker-model-name: " + rerankerModelName);
    sb.append(" | ");
	sb.append("test-reranker: " + testReranker);
    sb.append(" | ");
    sb.append("tune-reranker: " + tuneReranker);
    sb.append(" | ");
	sb.append("eval: " + eval);
	sb.append(" | ");
    sb.append("loss-type: " + lossType);
	sb.append(" | ");
	sb.append("use-averaging: " + useAveraging);
    sb.append(" | ");
	sb.append("reranker-iters: " + numRerankerIters);
    sb.append(" | ");
    sb.append("max-trees-per-sentence: " + maxTreesPerSentence);
    sb.append(" | ");
    sb.append("max-train-sentences: " + maxTrainSentences);
    sb.append(" | ");
    sb.append("thread-pool-size: " + threadPoolSize);
    sb.append(" | ");
	sb.append("base-feats: " + baseFeatures);
    sb.append(" | ");
    sb.append("kernel-order: " + kernelOrder);
    sb.append(" | ");
    sb.append("pa-type: " + PAType);
    sb.append(" | ");
    sb.append("base-score-param: " + Double.toString(baseScoreParam));
    sb.append(" | ");
    sb.append("lambda: " + Double.toString(lambda));
    sb.append(" | ");
    sb.append("learning-lambda: " + Double.toString(learningLambda));
    sb.append(" | ");
    sb.append("reranker-labeled-loss: " + rerankerLabeledLoss);
    sb.append(" | ");
    sb.append("format: " + format);
	sb.append(" | ");
    sb.append("outFormat: " + outFormat);
    sb.append(" | ");
    sb.append("reranker-type: " + rerankerType);
    sb.append("]\n");
	return sb.toString();
    }

    public RerankerTestOptions toRerankerTestOptions() {
        RerankerTestOptions rto = new RerankerTestOptions();
        rto.baseFeatures                = this.baseFeatures;
        rto.kernelOrder                 = this.kernelOrder;
        rto.rerankerPruneK              = this.rerankerPruneK;
        rto.useBaseScore                = this.useBaseSCore;
        rto.rerankerLabeledLoss         = this.rerankerLabeledLoss;
        rto.numRerankerIters            = this.numRerankerIters;
        rto.maxTreesPerSentence         = this.maxTreesPerSentence;
        rto.maxTrainSentences           = this.maxTrainSentences;
        rto.lossType                    = this.lossType;
        rto.lambda                      = this.lambda;

        return rto;
    }

    public void fromRerankerTestOptions(RerankerTestOptions rto) {
        
        this.baseFeatures                = rto.baseFeatures;
        this.kernelOrder                 = rto.kernelOrder;
        this.rerankerPruneK              = rto.rerankerPruneK;
        this.useBaseSCore                = rto.useBaseScore;
        this.rerankerLabeledLoss         = rto.rerankerLabeledLoss;
        this.numRerankerIters            = rto.numRerankerIters;
        this.maxTreesPerSentence         = rto.maxTreesPerSentence != null ? rto.maxTreesPerSentence : this.maxTreesPerSentence;
        this.maxTrainSentences           = rto.maxTrainSentences != null ? rto.maxTrainSentences : this.maxTrainSentences;
        this.lossType                    = rto.lossType;
        this.lambda                      = rto.lambda;
    }

    public void fromCommandLineArgs(String[] args) {

        if (args == null) {
            return;
        }
        for(int i = 0; i < args.length; i++) {
            String[] pair = args[i].split(":");

            if (pair[0].equals("train-reranker")) {
                trainReranker = true;
            }
            if (pair[0].equals("eval")) {
                eval = true;
            }
            if (pair[0].equals("test-reranker")) {
                testReranker = true;
            }
            if (pair[0].equals("tune-reranker")) {
                tuneReranker = true;
            }

            if (pair[0].equals("reranker-iters")) {
                numRerankerIters = Integer.parseInt(pair[1]);
            }
            if (pair[0].equals("max-trees-per-sentence")) {
                maxTreesPerSentence = Integer.parseInt(pair[1]);
            }
            if (pair[0].equals("max-train-sentences")) {
                maxTrainSentences = Integer.parseInt(pair[1]);
            }
            if (pair[0].equals("thread-pool-size")) {
                threadPoolSize = Integer.parseInt(pair[1]);
            }
            if (pair[0].equals("output-file")) {
                outfile = pair[1];
            }
            if (pair[0].equals("validation-file")) {
                validationFile = pair[1];
            }
            if (pair[0].equals("gold-file")) {
                goldfile = pair[1];
            }
            if (pair[0].equals("train-file")) {
                trainfile = pair[1];
            }
            if (pair[0].equals("test-file")) {
                testfile = pair[1];
            }
            if (pair[0].equals("reranker-model-name")) {
                rerankerModelName = pair[1];
            }
            if (pair[0].equals("base-score-param")) {
                baseScoreParam = Double.parseDouble(pair[1]);
            }
            if (pair[0].equals("lambda")) {
                lambda = Double.parseDouble(pair[1]);
            }
            if (pair[0].equals("learning-lambda")) {
                learningLambda = Double.parseDouble(pair[1]);
            }
            if (pair[0].equals("base-feats")) {
                baseFeatures = Integer.parseInt(pair[1]);
            }
            if (pair[0].equals("kernel-order")) {
                kernelOrder = Integer.parseInt(pair[1]);
            }
            if (pair[0].equals("pa-type")) {
                PAType = Integer.parseInt(pair[1]);
            }
            if (pair[0].equals("reranker-prune-k")) {
                rerankerPruneK = Integer.parseInt(pair[1]);
            }
            if (pair[0].equals("loss-type")) {
                lossType = pair[1];
            }
            if (pair[0].equals("reranker-labeled-loss") && pair[1].equals("true")) {
                rerankerLabeledLoss = true;
            }
            if (pair[0].equals("use-averaging") && pair[1].equals("true")) {
                useAveraging= true;
            }
            if (pair[0].equals("dumb-reranker")) {
                dumbReranker = pair[1].equals("true") ? true : false;
            }
            if (pair[0].equals("format")) {
                format = pair[1];
            }
            if (pair[0].equals("outFormat")) {
                outFormat = pair[1];
            }
            if (pair[0].equals("reranker-type")) {
                rerankerType = pair[1];
            }
            if (pair[0].equals("use-base-score")) {
                useBaseSCore = pair[1].equals("true") ? true : false;
            }
        }
    }
}

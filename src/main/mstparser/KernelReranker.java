package mstparser;

import mstparser.io.DependencyWriter;
import mstparser.io.MorphFeatTypes;
import utils.FastAlphabet;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static mstparser.KernelFeatureTemplate.getBits;

/**
 * Created with IntelliJ IDEA.
 * User: hillelt
 * Date: 10/19/13
 * Time: 3:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class KernelReranker {
    private static final Logger logger = Logger.getLogger(KernelReranker.class.getName());

    KernelRerankerPipe pipe = null;
    ParserOptions options = null;
    KernelParameters kernelParams;
    Parameters params;

    public Dataset trainset;
    public Dataset devset;

    public KernelRerankerPipe getPipe() {
        return pipe;
    }

    public KernelReranker(KernelRerankerPipe pipe, ParserOptions options) throws IOException {
        this.pipe = pipe;
        this.options = options;
        //logger.addHandler(new ConsoleHandler());
        logger.setLevel(Level.INFO);
        kernelParams = new KernelParameters(options.threadPoolSize, options.kernelOrder);
    }

    private class OutParams{
        public double objValue;
        public double normValue;
        public double lossValue;
        public double oracleLoss; // The number of edges that are different from the oracle per sentence.
        public double goldLoss; // The number of edges that are different from the gold per sentence.
        public double accuracy;
        public double randomAccuracy;
        public double dummyAccuracy;

        public OutParams() {

        }

        public OutParams(OutParams o) {
            this.objValue = o.objValue;
            this.normValue = o.normValue;
            this.lossValue = o.lossValue;
            this.oracleLoss = o.oracleLoss;
            this.goldLoss = o.goldLoss;
            this.accuracy = o.accuracy;
            this.randomAccuracy = o.randomAccuracy;
            this.dummyAccuracy = o.dummyAccuracy;
        }

        @Override
        public String toString() {
            return "OutParams{objValue=" + objValue +
                    ", normValue=" + normValue +
                    ", lossValue=" + lossValue +
                    ", oracleLoss=" + oracleLoss +
                    ", goldLoss=" + goldLoss +
                    ", accuracy=" + accuracy +
                    ", dummyAccuracy=" + dummyAccuracy +
                    ", randomAccuracy=" + randomAccuracy +
                    '}';
        }
    }

    /**
     * Calculates the score for a given instance in the k-best list.
     * @param dataset
     * @param sentenceNum
     * @param kBestIdx
     * @return
     */
    public double getScore(Dataset dataset, int sentenceNum, int kBestIdx, boolean useTrainCache) {
        DependencyInstance instance = dataset.get(sentenceNum).get(kBestIdx);
        try {
            return kernelParams.getScoreParallel(dataset, sentenceNum, kBestIdx, useTrainCache) + params.getScore(instance.fv);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        throw new RuntimeException();
    }

    public int argmaxScore(Dataset dataset, int sentenceNum, boolean useTrainCache) {
        return argmaxScore(dataset, sentenceNum, useTrainCache, false);
    }

    /**
     * Finds the index of the tree with the highest score for the sentence
     * with index sentenceNum among the candidates in the sentence k-best list.
     * @param dataset      the dataset
     * @return
     */
    public int argmaxScore(Dataset dataset, int sentenceNum, boolean useTrainCache, boolean useBaseScore) {
        double highestScore = Double.NEGATIVE_INFINITY;

        int highestScoringTreeIdx = -1;
        List<DependencyInstance> sentenceTrees = dataset.get(sentenceNum).getTrees();
        kernelParams.clearKBestCache();
        for (int i=1; i<sentenceTrees.size(); ++i) {
            double baseScore = useBaseScore ? options.baseScoreParam * dataset.get(sentenceNum).get(i).MSTScore : 0;
            double currentScore = baseScore + getScore(dataset, sentenceNum, i, useTrainCache);
            if (currentScore > highestScore) {
                highestScore = currentScore;
                highestScoringTreeIdx = i;
            }
        }
        kernelParams.clearKBestCache();
        assert(highestScoringTreeIdx > 0);
        return highestScoringTreeIdx;
    }

    public int augmentedArgmaxScore(Dataset dataset, int sentenceNum, boolean useTrainCache) {
        double highestScore = Double.NEGATIVE_INFINITY;
        int highestScoringTreeIdx = -1;
        List<DependencyInstance> sentenceTrees = dataset.get(sentenceNum).getTrees();
        kernelParams.clearKBestCache();
        DependencyInstance oracleInstance = dataset.getOracleInstance(sentenceNum);

        for (int i=1; i<sentenceTrees.size(); ++i) {
            DependencyInstance predInstance = dataset.get(sentenceNum).get(i);
            double currentScore = getScore(dataset, sentenceNum, i, useTrainCache);
            currentScore += oracleInstance.dist(predInstance, options.isNoPuncMode(), options.rerankerLabeledLoss)  ;
            if (currentScore > highestScore) {
                highestScore = currentScore;
                highestScoringTreeIdx = i;
            }
        }
        assert(highestScoringTreeIdx > 0);
        kernelParams.clearKBestCache();
        return highestScoringTreeIdx;
    }

    private DependencyInstance getHighestScoringInstance(Dataset dataset, int sentenceNum, boolean useBaseScore) {
        int bestInstanceIdx = argmaxScore(dataset, sentenceNum, false, useBaseScore);
        return dataset.get(sentenceNum).get(bestInstanceIdx);
    }

    private DependencyInstance getOracleInstance(DependencyInstance goldInstance, List<DependencyInstance> instances) {
        double bestDist=Double.POSITIVE_INFINITY;
        DependencyInstance oracleInstance = null;
        for (DependencyInstance instance : instances){
            double currDist = goldInstance.dist(instance, options.isNoPuncMode(), options.rerankerLabeledLoss);
            if (currDist < bestDist) {
                bestDist = currDist;
                oracleInstance = instance;

            }
        }
        assert(oracleInstance!=null);
        return oracleInstance;
    }

    private double calculatePATau(Parameters params,
                                  KernelParameters kernelParams,
                                  Dataset dataset,
                                  int sentenceNum,
                                  int oracleInstanceIdx,
                                  int predInstanceIdx,
                                  DependencyInstance oracleInstance,
                                  DependencyInstance predInstance) throws ExecutionException, InterruptedException {
        //double loss = Math.sqrt(oracleInstance.dist(predInstance, options.isNoPuncMode(), options.rerankerLabeledLoss));
        kernelParams.clearKBestCache();
        double loss = oracleInstance.dist(predInstance, options.isNoPuncMode(), options.rerankerLabeledLoss);
        double currentScore = kernelParams.getScoreParallel(dataset, sentenceNum, predInstanceIdx, true) -
                              kernelParams.getScoreParallel(dataset, sentenceNum, oracleInstanceIdx, true) +
                              params.getScore(predInstance.fv) - params.getScore(oracleInstance.fv);
        double delta = predInstance.fv.getDistVector(oracleInstance.fv).compactRepresentation().squreNorm();
        delta += multInstances(oracleInstance, oracleInstance) +
                 multInstances(predInstance, predInstance) -
                 2 * multInstances(oracleInstance, predInstance);
        double stepSize = ((loss+currentScore)/delta);
        kernelParams.clearKBestCache();
        //System.out.printf("%.6f\n", stepSize);
        //if (stepSize >= 1/(options.learningLambda * dataset.size())) {
        //    System.out.printf("capping at %.6f\n", 1/(options.learningLambda * dataset.size()));
        //}
        //assert(stepSize < 1/(options.learningLambda * dataset.size()));
        return Math.min(stepSize, 1/(options.learningLambda * dataset.size()));
    }



    // Uses the kernel to multiply the implicit kernel fv of two instances.
    private double multInstances(DependencyInstance inst1, DependencyInstance inst2) {
        double sum = 0.0;
        if (options.kernelOrder >= 1) {
            FirstOrderKernel firstOrderKernel = new FirstOrderKernel();

            for (int i=1; i<inst1.forms.length; ++i) {
                for (int j=1; j<inst2.forms.length; ++j) {
                    ArcPart arcPart1 = inst1.first_order_siblings[i];
                    ArcPart arcPart2 = inst2.first_order_siblings[j];
                    sum+= firstOrderKernel.sim(inst1, arcPart1, inst2, arcPart2);

                }
            }
        }
        return sum;
    }


    private void trainInternal(
                               Dataset                          dataset,
                               Dataset                          devset,
                               Dataset                          testset,
                               OutParams                        devOutParams,
                               OutParams                        testOutParams) throws IOException


    {


        int parametersSize;

        parametersSize = pipe.getDataAlphabetSize();

        params = new Parameters(parametersSize);
        params.setLoss(options.lossType);
        long start = System.currentTimeMillis();
        int chunkSize = 1000;
        double upd = options.numRerankerIters*dataset.size();
        int prevSupportSize = 0;
        OutParams optimalParams = null;
        int optimalIterations = -1;
        System.gc();
        for (int i=0; i<options.numRerankerIters*dataset.size(); ++i) {
            int instanceNum = i%dataset.size();
            if (instanceNum == 0) {
                logger.info(String.format("Iteration %d%n", i / dataset.size() + 1));
                int currentSupportSize = kernelParams.getSupportSize();
                logger.info(String.format("Support size: %d. New parts added: %d.%n", currentSupportSize, currentSupportSize - prevSupportSize));
                prevSupportSize = currentSupportSize;
            }

            // Add features based on manual-feature-templates to the candidate trees
            dataset.addFeaturesToTrees(dataset.get(instanceNum), pipe);

            List<DependencyInstance> sentenceTrees = dataset.get(instanceNum).getTrees();
            int j = -1;
            if (options.PAType == 1) {
                j = augmentedArgmaxScore(dataset, instanceNum, true);
            } else {
                j = argmaxScore(dataset, instanceNum, true);
            }

            int oracleInstanceIdx = dataset.getOracleInstanceIdx(instanceNum);
            //if (j!=oracleInstanceIdx) {
            if (sentenceTrees.get(oracleInstanceIdx).dist(sentenceTrees.get(j), options.isNoPuncMode(), options.rerankerLabeledLoss) > 0) {
                //assert(sentenceTrees.get(oracleInstanceIdx).dist(sentenceTrees.get(j), options.isNoPuncMode(), options.rerankerLabeledLoss) > 0);
                double tau=1.0;

                try {
                    //System.out.printf("inst num: %d\n", instanceNum);
                    tau = calculatePATau(params, kernelParams, trainset, instanceNum, oracleInstanceIdx, j,
                                   dataset.get(instanceNum).get(oracleInstanceIdx), dataset.get(instanceNum).get(j) );
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                kernelParams.update(instanceNum, j, tau, upd);
                //System.out.printf("j=%d, oracle=%d\n", j, oracleInstanceIdx);
                params.updateParamsPerceptron(dataset.get(instanceNum).get(oracleInstanceIdx), dataset.get(instanceNum).get(j), tau, upd);
            }
            --upd;

            // Remove features to free up memory.
            dataset.removeFeaturesFromTrees(dataset.get(instanceNum));



            if (((instanceNum+1)%dataset.size()) % chunkSize == 0) {
                int instancesProcessedInChunk = (instanceNum+1) % chunkSize == 0 ? chunkSize :  (instanceNum+1) % chunkSize;
                long end = System.currentTimeMillis();
                logger.info(String.format("Processed %d training instances (last %d took %d seconds)%n", instanceNum + 1, instancesProcessedInChunk, (end - start) / 1000));
                start = System.currentTimeMillis();
            }

            if (devset != null && (i+1)%dataset.size()==0) {

                logger.info("Completed Iteration, calculating loss");

                if (options.useAveraging) {
                    params.averageParams(upd, i+1);
                    kernelParams.averageParams(upd, i+1);
                }
                calcLoss(devset, devOutParams);
                logger.info(String.format("Iteration %d, accuracy=%.4f\n", (i+1)/dataset.size(), devOutParams.accuracy));
                if (optimalParams == null || devOutParams.accuracy > optimalParams.accuracy) {
                    optimalParams = new OutParams(devOutParams);
                    optimalIterations = (i+1)/dataset.size();
                }
                if (options.useAveraging) {
                    params.unaverageParams();
                    kernelParams.unaverageParams();
                }

            }

        }
        assert(upd==0);
        logger.info("Completed training iterations.");
        int currentSupportSize = kernelParams.getSupportSize();
        logger.info(String.format("Support size: %d. New parts added: %d.%n", currentSupportSize, currentSupportSize-prevSupportSize));




        if (options.useAveraging) {
            params.averageParams(upd, options.numRerankerIters*dataset.size());
            kernelParams.averageParams(upd, options.numRerankerIters*dataset.size());
        }
        kernelParams.prepareForTestPhase();
        if (devset != null) {
            System.out.printf(String.format("{\"reranker accuracy\":%.4f,\"dummy accuracy\":%.4f,\"random accuracy\":%.4f,\"optimal-iterations\":%d}", optimalParams.accuracy, optimalParams.dummyAccuracy, optimalParams.randomAccuracy, optimalIterations));
        }

        if (testset!=null) {
            logger.info("Calculating test set accuracy\n");
            calcLoss(testset, testOutParams, false, false);
            System.out.printf(String.format("{\"reranker accuracy\":%.4f,\"dummy accuracy\":%.4f,\"random accuracy\":%.4f}", testOutParams.accuracy, testOutParams.dummyAccuracy, testOutParams.randomAccuracy));
        }


    }

    public static DependencyInstance removeRoot(DependencyInstance instanceWithRoot, double score) {
        String[] goldFormsNoRoot = Arrays.copyOfRange(instanceWithRoot.forms, 1, instanceWithRoot.forms.length);
        String[] goldPosNoRoot = Arrays.copyOfRange(instanceWithRoot.postags, 1, instanceWithRoot.postags.length);
        String[] goldCPosNoRoot = Arrays.copyOfRange(instanceWithRoot.cpostags, 1, instanceWithRoot.cpostags.length);
        String[] goldLabelsNoRoot = Arrays.copyOfRange(instanceWithRoot.deprels, 1, instanceWithRoot.deprels.length);
        String[] goldLemmasNoRoot = Arrays.copyOfRange(instanceWithRoot.lemmas, 1, instanceWithRoot.lemmas.length);
        String[][] goldFeatsNoRoot = Arrays.copyOfRange(instanceWithRoot.feats, 1, instanceWithRoot.feats.length);
        int[] goldHeadsNoRoot = Arrays.copyOfRange(instanceWithRoot.heads, 1, instanceWithRoot.heads.length);
        return new DependencyInstance(goldFormsNoRoot, goldCPosNoRoot, goldPosNoRoot, goldLemmasNoRoot, goldLabelsNoRoot, goldHeadsNoRoot, goldFeatsNoRoot, score);
    }

    private void calcLoss(Dataset dataset, OutParams outParams) throws IOException {
        calcLoss(dataset, outParams, false, false);
    }

    private void calcLoss(Dataset dataset, OutParams outParams, boolean useBaseScore, boolean writeOutput) throws IOException {

        int totalEdges = 0;
        int incorrectEdges = 0;
        int incorrectDummyEdges = 0;
        int incorrectRandomEdges = 0;
        long start = System.currentTimeMillis();
        int chunkSize = 100;
        DependencyWriter depWriter = null;
        if (writeOutput) {
            depWriter =
                    DependencyWriter.createDependencyWriter("CONLL", true);
            depWriter.startWriting(options.outfile);
        }
        for (int i=0; i < dataset.size(); ++i) {
            if (i % chunkSize == 0) {
                long end = System.currentTimeMillis();
                logger.info(String.format("Processed %d dev instances (last %d took %d seconds)%n", i, chunkSize, (end - start) / 1000));
                start = System.currentTimeMillis();
            }
            List<DependencyInstance> instances = dataset.get(i).getTrees();

            dataset.addFeaturesToTrees(instances, pipe);


            DependencyInstance goldInstance = instances.get(0);
            List<DependencyInstance> kbestInstances = instances.subList(1, instances.size());
            DependencyInstance oracleInstance = dataset.getOracleInstance(i);
            DependencyInstance bestInstance = getHighestScoringInstance(dataset, i, useBaseScore);
            if (depWriter != null) {
                if (options.dumbReranker) {

                    depWriter.write(KernelReranker.removeRoot(instances.get(1), instances.get(1).MSTScore));
                } else {
                    depWriter.write(KernelReranker.removeRoot(bestInstance, bestInstance.MSTScore));
                }
            }
            DependencyInstance dummyInstance = kbestInstances.get(0);

            Random rand1 = new Random();
            rand1.setSeed(123);
            DependencyInstance randomInstance = kbestInstances.get(rand1.nextInt(kbestInstances.size()));
            double realLoss = goldInstance.dist(bestInstance, options.isNoPuncMode(), options.rerankerLabeledLoss);
            double  oracleLoss = oracleInstance.dist(bestInstance, options.isNoPuncMode(), options.rerankerLabeledLoss);
            double dummyLoss = goldInstance.dist(dummyInstance, options.isNoPuncMode(), options.rerankerLabeledLoss);
            double randomLoss = goldInstance.dist(randomInstance, options.isNoPuncMode(), options.rerankerLabeledLoss);
            double edgesInInstance = goldInstance.totalArcsNoPunct(); // We don't count the root node or punctuation.
            totalEdges+=edgesInInstance;
            incorrectEdges += realLoss;
            outParams.oracleLoss += oracleLoss / (double)dataset.size();
            outParams.goldLoss += realLoss / (double)dataset.size();
            incorrectDummyEdges += dummyLoss;
            incorrectRandomEdges += randomLoss;

            dataset.removeFeaturesFromTrees(instances);


        }
        if (depWriter != null) {
            depWriter.finishWriting();
        }


        if (dataset.size() != 0) {

            outParams.accuracy = (double)(totalEdges-incorrectEdges)/(double)totalEdges;
            outParams.dummyAccuracy = (double)(totalEdges-incorrectDummyEdges)/(double)totalEdges;
            outParams.randomAccuracy = (double)(totalEdges-incorrectRandomEdges)/(double)totalEdges;


        }

    }

    public double tune() throws IOException {



        logger.info("Reading the validation instances ... ");
        Dataset validationSet = new Dataset(options.validationFile, this.options, pipe);
        validationSet.addFeaturesToAllSentences(pipe);

        logger.info("Testing with different base-score-param values...");
        double bestScore=Double.NEGATIVE_INFINITY;
        double bestBaseScoreParam = Double.NaN;
        for (double i=0.0; i<3.05; i+=0.05) {
            options.baseScoreParam = i;
            OutParams outParams = new OutParams();
            calcLoss(validationSet, outParams, true, false);
            logger.info(String.format("base score param=%.2f, score=%.4f", i, outParams.accuracy));
            if (outParams.accuracy > bestScore) {
                bestScore = outParams.accuracy;
                bestBaseScoreParam = i;
            }
        }
        System.out.printf("{\"best param\":%.2f,\"Best score\":%.4f}", bestBaseScoreParam, bestScore);
        logger.info("done.");


        return 0.0;
    }

    public double test() throws IOException {


        long startTime = System.currentTimeMillis();
        logger.info("Reading the test instances ... ");
        Dataset testset = new Dataset(options.testfile, this.options, pipe);
        OutParams testOutParams = new OutParams();
        calcLoss(testset, testOutParams, options.useBaseSCore, true);
        System.out.printf(String.format("{\"reranker accuracy\":%.4f,\"dummy accuracy\":%.4f,\"random accuracy\":%.4f}", testOutParams.accuracy, testOutParams.dummyAccuracy, testOutParams.randomAccuracy));
        long endTime = System.currentTimeMillis();
        double speed = testset.getTotalToks()/(double)((endTime-startTime)/1000.0);
        logger.info(String.format("Completed parsing test. Speed: %.2f toks/sec\n", speed));
        return testOutParams.accuracy;
    }



    public double train(String trainfile) throws IOException {
        logger.info("Creating dictionaries for train instances ...\n");
        this.pipe.createDictionaries(options.trainfile, false);
        logger.info("Done.\n");


        logger.info("Reading the training instances ...\n");
        trainset = new Dataset(options.trainfile, this.options, pipe);
        Dataset testset = null;

        File validationFile = new File(options.validationFile);
        File testFile = new File(options.testfile);
        if (validationFile.exists()) {
            logger.info("Creating dictionaries for dev instances ...\n");
            this.pipe.createDictionaries(options.validationFile, true);
            logger.info("Done.\n");
            logger.info("Reading the validation instances ...\n");
            devset = new Dataset(options.validationFile, this.options, pipe);
            System.gc();
        }
        if (testFile.exists()) {
            logger.info("Creating dictionaries for test instances ...\n");
            this.pipe.createDictionaries(options.testfile, true);
            logger.info("Done.\n");
            logger.info("Reading the test instances ...\n");
            testset = new Dataset(options.testfile, this.options, pipe);
            System.gc();
        }

        kernelParams = new KernelParameters(trainset, options.threadPoolSize, options.kernelOrder);

        pipe.createAlphabet(trainset);

        OutParams devOutParams = new OutParams();
        OutParams testOutParams = new OutParams();

        trainInternal(trainset, devset, testset, devOutParams, testOutParams);
        return devOutParams.accuracy;

    }

    public void shutdownThreads() {
        kernelParams.ShutdownThreads();
    }

    public void saveModel(String file) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
        out.writeObject(options.toRerankerTestOptions());
        out.writeObject(params.parameters);
        out.writeObject(MorphFeatTypes.INSTANCE.featsAlphabet);
        out.writeObject(kernelParams.firstOrderSupportParts);
        out.writeObject(pipe.getDataAlphabet());
        out.writeObject(pipe.getTypeAlphabet());
        out.writeObject(pipe.wordDictionary);
        out.writeObject(pipe.tagDictionary);

        out.close();

    }

    public void loadModel(String file) throws Exception {

        logger.info(file + "\n");

        ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
        RerankerTestOptions rto = (RerankerTestOptions)in.readObject();
        params =  new Parameters((double[]) in.readObject());

        MorphFeatTypes.INSTANCE.featsAlphabet = (Alphabet) in.readObject();
        kernelParams.firstOrderSupportParts = (List<SupportPart>) in.readObject();
        kernelParams.prepareForTestPhase();
        FastAlphabet dataAlphabet = (FastAlphabet) in.readObject();
        FastAlphabet typeAlphabet = (FastAlphabet) in.readObject();
        pipe.dataAlphabet = dataAlphabet;
        pipe.typeAlphabet = typeAlphabet;
        pipe.wordDictionary = (Dictionary) in.readObject();
        pipe.tagDictionary = (Dictionary) in.readObject();
        pipe.wordNumBits = getBits(pipe.wordDictionary.size() + 1);
        pipe.tagNumBits = getBits(pipe.tagDictionary.size() + 1);
        in.close();
        pipe.closeAlphabets();
        options.fromRerankerTestOptions(rto);
        options.fromCommandLineArgs(options.args);
        kernelParams.setKernelOrder(options.kernelOrder);
        logger.info("Reading the training instances ... ");
        trainset = new Dataset(options.trainfile, this.options, pipe);
        kernelParams.trainDataset = trainset;
        params.setLoss(options.lossType);


    }

    public double[] getParams() {

        throw new UnsupportedOperationException();

    }

}

///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2007 University of Texas at Austin and (C) 2005
// University of Pennsylvania and Copyright (C) 2002, 2003 University
// of Massachusetts Amherst, Department of Computer Science.
//
// This software is licensed under the terms of the Common Public
// License, Version 1.0 or (at your option) any subsequent version.
// 
// The license is approved by the Open Source Initiative, and is
// available from their website at http://www.opensource.org.
///////////////////////////////////////////////////////////////////////////////

package mstparser.io;

import mstparser.DependencyInstance;
import mstparser.RelationalFeature;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RerankerReader extends CONLLReader {

    private static final Pattern rankPattern = Pattern.compile("#\\s+rank:\\s+(\\d+)");
    private static final Pattern scorePattern = Pattern.compile("#\\s+score:\\s+(-?\\d+(.\\d+)?)");

    public RerankerReader(boolean discourseMode) {
        super(discourseMode);
    }

    public DependencyInstance getNext() throws IOException {
        String line = inputReader.readLine();
        if (line == null || line.equals("")) {
            inputReader.close();
            return null;
        }

        Matcher rankMatcher = rankPattern.matcher(line);
        if (rankMatcher.find()) {
            int instanceRank = Integer.parseInt(rankMatcher.group(1));
            line = inputReader.readLine();
            Matcher scoreMatcher = scorePattern.matcher(line);
            scoreMatcher.find();
            double instanceScore = Double.parseDouble(scoreMatcher.group(1));
            DependencyInstance instance = super.getNext();
            instance.MSTScore = instanceScore;
            instance.rank = instanceRank;
            instance.initializeDeps();
            return instance;
        } else {
            // Perhaps old input format? support it...
            double instanceScore = Double.parseDouble(line);
            DependencyInstance instance = super.getNext();
            instance.MSTScore = instanceScore;
            instance.initializeDeps();
            return instance;
        }

    }
}

package mstparser.io;

import mstparser.DependencyInstance;
import mstparser.Util;

import java.io.IOException;
import java.text.DecimalFormat;

/**
 * Created with IntelliJ IDEA.
 * User: hillelt
 * Date: 10/20/13
 * Time: 12:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class RerankerWriter extends CONLLWriter{
    public RerankerWriter(boolean labeled) {
        super(labeled);
    }

    public void write(DependencyInstance instance) throws IOException {

        DecimalFormat df = null;

        df = new DecimalFormat();
        df.setMaximumFractionDigits(4);

        if (!Util.doubleEquals(instance.MSTScore, -1000.0)) {
            String score = df.format(instance.MSTScore);
            score = score.replaceAll(",", "");
            writer.write(score);
            writer.newLine();
        } else {
            throw new IOException("MST Score was not set by the parser. Run the parser test with format:reranker.");
        }
        super.write(instance);

    }

}

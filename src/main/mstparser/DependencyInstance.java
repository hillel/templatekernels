package mstparser;

import utils.FastAlphabet;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class DependencyInstance implements Serializable {
    public int rank = -1; // Position in the k-best list. 0 is the gold tree.
    public double MSTScore = -1000.0;
    public FeatureVector fv;
    public String actParseTree;

    // The various data types. Here's an example from Portuguese:
    //
    // 3  eles ele   pron       pron-pers M|3P|NOM 4    SUBJ   _     _
    // ID FORM LEMMA COURSE-POS FINE-POS  FEATURES HEAD DEPREL PHEAD PDEPREL
    //
    // We ignore PHEAD and PDEPREL for now. 

    // FORM: the forms - usually words, like "thought"
    public String[] forms;

    public boolean[] isPunct;

    // LEMMA: the lemmas, or stems, e.g. "think"
    public String[] lemmas;

    // COURSE-POS: the course part-of-speech tags, e.g."V"
    public String[] cpostags;

    // FINE-POS: the fine-grained part-of-speech tags, e.g."VBD"
    public String[] postags;

    // FEATURES: some features associated with the elements separated by "|", e.g. "PAST|3P"
    public String[][] feats;

    // HEAD: the IDs of the heads for each element
    public int[] heads;

    public int[][] deps;

    // first order siblings per modifier
    public ArcPart[] first_order_siblings;

    // Span start: for each token, what's the leftest point in its span
    public int[] st;

    // Span start: for each token, what's the rightest point in its span
    public int[] en;

    public int[] formids;
    public int[] lemmaids;
    public int[] postagids;
    public int[] cpostagids;
    public int[] deprelids;
    public int[] deplbids;
    public int[][] featids;
    public long[][] nodeFeats;
    public long[][] edgeFeats;

    // DEPREL: the dependency relations, e.g. "SUBJ"
    public String[] deprels;

    // RELATIONAL FEATURE: relational features that hold between items
    public RelationalFeature[] relFeats;
	
    // Confidence scores per edge
    public double[] confidenceScores;



    public DependencyInstance() {}

    public DependencyInstance(DependencyInstance source) {
        this.fv = source.fv;
        this.actParseTree = source.actParseTree;

    }
    
    public DependencyInstance(String[] forms, FeatureVector fv) {
        this.forms = forms;
        this.fv = fv;
        initIsPunct();
    }
    
    public DependencyInstance(String[] forms, String[] postags, FeatureVector fv) {
        this(forms, fv);
        this.postags = postags;
        initIsPunct();
    }
    
    public DependencyInstance(String[] forms, String[] postags, 
			      String[] labs, FeatureVector fv) {
        this(forms, postags, fv);
        this.deprels = labs;
        initIsPunct();
    }

    public DependencyInstance(String[] forms, String[] postags, 
			      String[] labs, int[] heads) {
        this.forms = forms;
        this.postags = postags;
        this.deprels = labs;
        this.heads = heads;
        initIsPunct();
    }

    public DependencyInstance(String[] forms, String[] cpostags, String[] postags,
                              String[] labs, int[] heads, double score) {
        this.forms = forms;
        this.cpostags = cpostags;
        this.postags = postags;
        this.deprels = labs;
        this.heads = heads;
        this.MSTScore = score;
        initIsPunct();
    }



    public DependencyInstance(String[] forms, String[] postags,
                              String[] labs, int[] heads, double score) {
        this.forms = forms;
        this.postags = postags;
        this.deprels = labs;
        this.heads = heads;
        this.MSTScore = score;
        initIsPunct();
    }


    public DependencyInstance(String[] forms, String[] cpostags, String[] postags,
                              String[] lemmas, String[] labs, int[] heads, double score) {
        this.forms = forms;
        this.cpostags = cpostags;
        this.postags = postags;
        this.lemmas = lemmas;
        this.deprels = labs;
        this.heads = heads;
        this.MSTScore = score;
        this.feats = new String[this.forms.length][0];
        initIsPunct();
    }

    public DependencyInstance(String[] forms, String[] cpostags, String[] postags,
                              String[] lemmas, String[] labs, int[] heads, String[][] feats, double score) {
        this.forms = forms;
        this.cpostags = cpostags;
        this.postags = postags;
        this.lemmas = lemmas;
        this.deprels = labs;
        this.heads = heads;
        this.MSTScore = score;
        this.feats = feats;
        initIsPunct();
    }


    public DependencyInstance(String[] forms, String[] postags, 
		      String[] labs, int[] heads, double[] confidenceScores) {
        this(forms, postags, labs, heads);
        this.confidenceScores = confidenceScores;
        initIsPunct();
	}
    
    public DependencyInstance(String[] forms, String[] lemmas, String[] cpostags, 
			      String[] postags, String[][] feats, String[] labs, int[] heads) {
        this(forms, postags, labs, heads);
        this.lemmas = lemmas;
        this.cpostags = cpostags;
        this.feats = feats;
        initIsPunct();
    }
    
    public DependencyInstance(String[] forms, String[] lemmas, String[] cpostags, 
		      String[] postags, String[][] feats, String[] labs, int[] heads,
		      RelationalFeature[] relFeats, double[] confidenceScores) {
        this(forms, lemmas, cpostags, postags, feats, labs, heads);
        this.relFeats = relFeats;
        this.confidenceScores = confidenceScores;
        initIsPunct();
    }

    public void initIsPunct() {
        isPunct = new boolean[forms.length];
        for (int i=0; i<forms.length; ++i) {
            if (postags[i].equals("XP") /* Danish */ ||
                postags[i].equals("XS") /* Danish */ ||
                postags[i].equals("G") /* Arabic */ ||
                postags[i].equals("PUNC") /* Slovene */ ||
                postags[i].equals("Punct") /* Bulgarian */ ||
                (cpostags != null && cpostags[i].equals("."))) /* English */ {
                if (!Pattern.matches("\\p{Punct}", forms[i]) && !forms[i].equals("...")) {
                   // System.err.printf("WARNING: punctuation outside the known forms: %s%n", forms[i]);
                }

                isPunct[i] = true;
            } else {
                if (Pattern.matches("\\p{Punct}", forms[i]) && !forms[i].equals("&")) {

                   // System.err.printf("WARNING: punctuation not identified by POS: %s%n", forms[i]);
                }
                isPunct[i] = false;
            }
        }
    }

    public void setInstIds(Dictionary tagDict, Dictionary wordDict, FastAlphabet typeAlphabet) {
        int length = forms.length;
        formids = new int[length];
        deprelids = new int[length];
        deplbids = new int[length];
        postagids = new int[length];
        cpostagids = new int[length];

        for (int i = 0; i < length; ++i) {
            formids[i] = wordDict.lookupIndex("form="+forms[i]);
            postagids[i] = tagDict.lookupIndex("pos="+postags[i]);
            cpostagids[i] = tagDict.lookupIndex("cpos="+cpostags[i]);
            deprelids[i] = tagDict.lookupIndex("label="+deprels[i]);
            deplbids[i] = typeAlphabet.lookupIndex(deprelids[i]);

        }

        if (lemmas != null) {
            lemmaids = new int[length];
            for (int i = 0; i < length; ++i)
                lemmaids[i] = wordDict.lookupIndex("lemma="+lemmas[i]);
        }

        featids = new int[length][];
        for (int i = 0; i < length; ++i) if (feats[i] != null) {
            featids[i] = new int[feats[i].length];
            for (int j = 0; j < feats[i].length; ++j)
                featids[i][j] = tagDict.lookupIndex("feat="+feats[i][j]);
        }


    }


    public void initializeDeps(){
        deps = new int[heads.length][];
        ArrayList<ArrayList<Integer>> depsList = new ArrayList<ArrayList<Integer>>();
        for (int i=0; i<heads.length; ++i) {
            depsList.add(new ArrayList<Integer>());
        }
        for (int i=0; i<heads.length; ++i) {
            int head = heads[i];
            if (head != -1) {
                depsList.get(head).add(i);
            }
        }

        for (int i=0; i<heads.length; ++i) {
            ArrayList<Integer> currDepsList = depsList.get(i);
            deps[i] = new int[currDepsList.size()];
            for (int j=0; j<currDepsList.size(); ++j) {
                deps[i][j] = currDepsList.get(j);
            }

        }

        initializeSpans();
        initializeSiblingArrays();
    }

    /**
     * Gets all the siblings up to the order.
     * @param firstModifier
     * @return
     */
    public int[] getSiblings(int firstModifier, int order) {
        if (firstModifier == 0) {
            return null;
        }
        int[] siblings = new int[order];
        int head = heads[firstModifier];
        int[] head_deps = deps[head];
        boolean foundModifier = false;
        int cnt=0;
        boolean leftArcs = (firstModifier < head);
        for (int i=0; i<head_deps.length && cnt < order ; ++i) {
            if (head_deps[i] == firstModifier) {
                foundModifier = true;
            }
            if (foundModifier) {
                /*
                // Only add siblings that are on the same side.
                if ((leftArcs && (head_deps[i] < head)) ||
                    (!leftArcs) && (head_deps[i] > head)) {
                */
                    siblings[cnt]=head_deps[i];
                    cnt++;
                /*
                }
                */


            }
        }

        return cnt==order ? siblings : null;

    }

    public void initializeSiblingArrays() {

        this.first_order_siblings = new ArcPart[this.forms.length];

        for (int i = 1; i < this.forms.length; i++){
            int m = i;
            int h = heads[m];
            this.first_order_siblings[m]= new ArcPart(h, m);

        }

    }

    public void initializeSpans() {
        this.st = new int[this.forms.length];
        this.en = new int[this.forms.length];

        for (int i = 0; i < this.forms.length; i++)
            if (this.en[i] == 0)
                initializeSpan(i);
    }

    public void initializeSpan(int id) {
        this.st[id] = id;
        this.en[id] = id + 1;
        for (int i = 0; i < this.deps[id].length; i++) {
            int cid = this.deps[id][i];
            if (this.en[cid] == 0)
                initializeSpan(cid);
            if (this.st[cid] < this.st[id])
                this.st[id] = this.st[cid];
            if (this.en[cid] > this.en[id])
                this.en[id] = this.en[cid];
        }
    }

    public void setFeatureVector (FeatureVector fv) {
	this.fv = fv;
    }


    public int length () {
	return forms.length;
    }
    public int totalArcsNoPunct () {
        // We start from 1 to ignore the root. Only works if there's a root.
        int len =0;
        for (int i=1; i<deprels.length; i++) {
            if (!this.isPunct[i]) {
                ++len;
            }
        }
        return len;
    }

    public String toString () {
	StringBuffer sb = new StringBuffer();
	sb.append(Arrays.toString(forms)).append("\n");
	return sb.toString();
    }

    public  boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public boolean equalsJustWords(DependencyInstance other) {
        if (other == null) {
            return false;
        }
        if (forms.length != other.forms.length) {
            return false;
        }

        for (int i=0; i<forms.length; ++i) {
            if ((forms[i].equals("<num>") && isNumeric(other.forms[i]))
                    || other.forms[i].equals("<num>") && isNumeric(forms[i])) {
                continue;
            }
            if (!forms[i].equals(other.forms[i])){
                return false;
            }
        }
        return true;
    }

    public boolean equalsIgnoreLabels(DependencyInstance other) {
        if (other == null) {
            return false;
        }
        if (heads.length != other.heads.length) {
            return false;
        }

        if (!this.equalsJustWords(other)) {
            return false;
        }

        for (int i=0; i<heads.length; ++i) {
            if (heads[i]!=other.heads[i]){
                return false;
            }
        }
        return true;
    }

    //public int dist(DependencyInstance other){
    //    return this.dist(other, false, true);
    //}

    public String getSentence() {
        StringBuilder sb = new StringBuilder();
        if (forms.length > 0) {
            sb.append(forms[0]);
            for (int i=1; i<forms.length; ++i) {
                sb.append(" ");

                sb.append(forms[i]);
            }
        }
        return sb.toString();
    }

    /**
     *
     * @param other
     * @param noPunc don't count differences that are the result of different head for punct nodes.
     * @return
     */
    public double dist(DependencyInstance other, boolean noPunc, boolean labeled){
        double dist = 0;
        if (this.heads.length != other.heads.length) {
            throw new IllegalArgumentException("other is not the same size as this.");
        }

        for (int i=0; i<this.heads.length; i++) {
            // If there's a root -- skip it.
            if (i==0 && this.heads[i]==-1) {
                continue;
            }

            if (this.heads[i] != other.heads[i]) {
                if (!noPunc || !this.isPunct[i] ){
                    ++dist;
                }

            }
            if (labeled && this.deprels[i]!= other.deprels[i]) {
                if (!noPunc || !this.isPunct[i]) {
                   dist+=0.5;
                }
            }
        }

        return dist;
    }

    private void writeObject (ObjectOutputStream out) throws IOException {
	out.writeObject(forms);
	out.writeObject(lemmas);
	out.writeObject(cpostags);
	out.writeObject(postags);
	out.writeObject(heads);
	out.writeObject(deprels);
	out.writeObject(actParseTree);
	out.writeObject(feats);
	out.writeObject(relFeats);
    out.writeObject(MSTScore);
    }


    private void readObject (ObjectInputStream in) throws IOException, ClassNotFoundException {
	forms = (String[])in.readObject();
	lemmas = (String[])in.readObject();
	cpostags = (String[])in.readObject();
	postags = (String[])in.readObject();
	heads = (int[])in.readObject();
	deprels = (String[])in.readObject();
	actParseTree = (String)in.readObject();
	feats = (String[][])in.readObject();
	relFeats = (RelationalFeature[])in.readObject();
    MSTScore = (double)in.readObject();
    }

}

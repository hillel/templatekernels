package mstparser;


import java.io.*;
import java.util.*;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class DependencyParser {
    private static final Logger logger = Logger.getLogger(KernelReranker.class.getName());

    public ParserOptions options;



    static Random rand = new Random();


    public DependencyParser(ParserOptions options) {
        rand.setSeed(123);
    }


    /////////////////////////////////////////////////////
    // RUNNING THE RERANKER
    ////////////////////////////////////////////////////
    public static void main(String[] args) throws Exception {
        ParserOptions options = new ParserOptions(args);
        Logger globalLogger = Logger.getLogger("");
        Handler[] handlers = globalLogger.getHandlers();
        for(Handler handler : handlers) {
            handler.setFormatter(new MyLogFormatter());
        }
        if (options.trainReranker) {
            KernelRerankerPipe pipe = new KernelRerankerPipe(options);
            KernelReranker reranker = new KernelReranker(pipe, options);

            reranker.train(options.trainfile);

            logger.info("Saving model...\n");
            reranker.saveModel(options.rerankerModelName);
            logger.info("done.\n");

            reranker.shutdownThreads();

        }

        if (options.tuneReranker) {
            KernelRerankerPipe pipe = new KernelRerankerPipe(options);

            KernelReranker reranker = new KernelReranker(pipe, options);;
            System.out.print("\tLoading reranking model... ");

            reranker.loadModel(options.rerankerModelName);

            reranker.tune();

            System.out.println("done.\n");
            reranker.shutdownThreads();

        }

        if (options.testReranker) {
            KernelRerankerPipe pipe = new KernelRerankerPipe(options);

            KernelReranker reranker = new KernelReranker(pipe, options);
            logger.info("\tLoading reranking model... ");

            reranker.loadModel(options.rerankerModelName);
            reranker.test();

            logger.info("done.\n");

            reranker.shutdownThreads();
        }

        System.out.println();

        if (options.eval) {
            System.out.println("\nEVALUATION PERFORMANCE:");
            DependencyEvaluator.evaluate(options.goldfile,
                    options.outfile,
                    options.format);
        }

    }


}

package mstparser;


import gnu.trove.list.array.TDoubleArrayList;
import mstparser.io.MorphFeatTypes;
import utils.FastAlphabet;
import gnu.trove.list.array.TLongArrayList;
import mstparser.io.RerankerReader;
import utils.Utils;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Logger;

import static mstparser.FeatureTemplate.Arc.*;
import static mstparser.FeatureTemplate.Word.numWordFeatBits;
import static mstparser.KernelFeatureTemplate.*;
import static mstparser.KernelFeatureTemplate.NodeFT.numNodeFeatBits;


/**
 * Fast third order pipe. Emphasis on speed.
 */
public class KernelRerankerPipe {
    private static final Logger logger = Logger.getLogger(KernelReranker.class.getName());

    public static int TOKEN_START = 1;
    public static int TOKEN_MID = 2;
    public static int TOKEN_END = 3;

    // for punc
    public static int TOKEN_QUOTE = 4;
    public static int TOKEN_RRB = 5;
    public static int TOKEN_LRB = 6;

    public Dictionary tagDictionary = new Dictionary();
    public Dictionary wordDictionary = new Dictionary();
    public int tagNumBits, wordNumBits, disNumBits = 4;
    boolean labeled;

    protected FastAlphabet dataAlphabet;

    protected FastAlphabet typeAlphabet;   // maps the different dependency labels into numerical ids

    public FastAlphabet getTypeAlphabet() {
        return typeAlphabet;
    }

    public FastAlphabet getDataAlphabet() {
        return dataAlphabet;
    }

    public int getDataAlphabetSize() {
        return dataAlphabet.size();
    }


    ParserOptions options;
    public static int maxLenDiff = 4;

    public int numArcFeats;					// number of arc structure features
    public int numWordFeats;				// number of word features

    public KernelRerankerPipe(ParserOptions options) throws IOException, ClassNotFoundException {

        this.options = options;
        this.dataAlphabet = new FastAlphabet();
        this.typeAlphabet = new FastAlphabet();
    }

    public KernelRerankerPipe(ParserOptions options, FastAlphabet dataAlphabet, FastAlphabet typeAlphabet) throws IOException, ClassNotFoundException {

        this.options = options;
        this.dataAlphabet = dataAlphabet;
        this.typeAlphabet = typeAlphabet;
    }

    public static long getCodeP(NodeFT temp, long p) {
        return (p << NodeFT.numNodeFeatBits) | temp.ordinal();
    }
    public static long getCodeC(NodeFT temp, long c) {
        return (c << NodeFT.numNodeFeatBits) | temp.ordinal();
    }

    public static long getCodeW(NodeFT temp, long w) {
        return (w << NodeFT.numNodeFeatBits) | temp.ordinal();
    }

    public static long getCodeF(NodeFT temp, long f) {
        return (f << NodeFT.numNodeFeatBits) | temp.ordinal();
    }

    public static long getCodePP(NodeFT temp, long p1, long p2) {
        return (((p1 << midOff) | p2) << NodeFT.numNodeFeatBits) | temp.ordinal();
    }

    public static long getCodePW(NodeFT temp, long p, long w) {
        return (((w << midOff) | p) << NodeFT.numNodeFeatBits) | temp.ordinal();
    }

    public static long getCodePF(NodeFT temp, long p, long f) {
        return (((p << midOff) | f) << NodeFT.numNodeFeatBits) | temp.ordinal();
    }

    public static long getCodePPP(NodeFT temp, long p1, long p2, long p3) {
        return (((((p1 << midOff) | p2) << midOff) | p3) << NodeFT.numNodeFeatBits) | temp.ordinal();
    }

    public static long getCodeLab(EdgeFT temp, long lab) {
        return (lab << EdgeFT.numEdgeFeatBits) << temp.ordinal();
    }

    public static long getCodeP(EdgeFT temp, long p) {
        return (p << EdgeFT.numEdgeFeatBits) << temp.ordinal();
    }

    public static long getCodeLabP(EdgeFT temp, long lab, long p) {
        return (((lab << midOff) | p) << EdgeFT.numEdgeFeatBits) | temp.ordinal();
    }

    /**
     * len is large-small
     */
    public static long getCodeLen(EdgeFT temp, long len) {
        len = binDist(len);
        return (len << EdgeFT.numEdgeFeatBits) | temp.ordinal();
    }

    public static long getCodeDir(EdgeFT temp, long dir) {
        return (dir << EdgeFT.numEdgeFeatBits) | temp.ordinal();
    }

    public static long getCodeLabDir(EdgeFT temp, long lab, long dir) {

        return (((lab << 2) | dir) << EdgeFT.numEdgeFeatBits) | temp.ordinal();
    }

    public static long getCodeLenDir(EdgeFT temp, long len, long dir) {
        len = binDist(len);
        return (((len << 2) | dir) << EdgeFT.numEdgeFeatBits) | temp.ordinal();
    }

    public static long getCodeLabLenDir(EdgeFT temp, long lab,long len, long dir) {
        len = binDist(len);
        return ((((lab << midOff)| (len << 2)) | dir) << EdgeFT.numEdgeFeatBits) | temp.ordinal();
    }



    // dist is large-small
    private static long binDist(long dist) {
        // Binning the distance (0,1,2,3,4,5-10,>10). 0 is for adjacent words.
        long binnedDist = -1;
        if (dist > 10) {
            binnedDist = 6;
        } else if (dist > 5) {
            binnedDist = 5;
        } else {
            binnedDist = dist - 1;
        }
        assert(binnedDist>=0);
        return binnedDist;
    }

    public void createDictionaries(String file, boolean stopGrowth) throws IOException {
        long start = System.currentTimeMillis();
        // all forms, lemmas, pos etc should be 1-base indexed.
        //tagDictionary.lookupIndex("#ZERO_INDEX#");
        //wordDictionary.lookupIndex("#ZERO_INDEX#");

        // special symbols used in features
        TOKEN_START = tagDictionary.lookupIndex("#TOKEN_START#");
        TOKEN_MID = tagDictionary.lookupIndex("#TOKEN_MID#");
        TOKEN_END = tagDictionary.lookupIndex("#TOKEN_END#");
        wordDictionary.lookupIndex("#TOKEN_START#");
        wordDictionary.lookupIndex("#TOKEN_MID#");
        wordDictionary.lookupIndex("#TOKEN_END#");
        wordDictionary.lookupIndex("form=\"");
        wordDictionary.lookupIndex("form=)");
        wordDictionary.lookupIndex("form=(");
        RerankerReader depReader = new RerankerReader(false);
        labeled = depReader.startReading(file);
        int sentencesRead = 0;
        DependencyInstance instance = depReader.getNext();
        int currSentenceTrees = 0;
        int num=0;
        while(instance != null) {
            num++;
            if (num % 10000==0) {
                logger.info(num + " ");
            }
            ++currSentenceTrees;


            if (instance.rank == 0 || /* backword compatability to old format */ !Util.doubleEquals(instance.MSTScore, -500)) {
                if (currSentenceTrees > options.rerankerPruneK) {
                    instance = depReader.getNext();
                    continue;
                }

            } else {
                currSentenceTrees = 1;
                sentencesRead ++;
                if (sentencesRead > options.maxTrainSentences) {
                    System.out.println("");
                    System.out.println(String.format("Dictionary creation stopped since we reached maxTrainSentences=%d", options.maxTrainSentences));
                    break;
                }
            }

            instance.setInstIds(tagDictionary, wordDictionary, typeAlphabet);
            instance = depReader.getNext();
        }
        if (stopGrowth) {
            wordDictionary.stopGrowth();
            tagDictionary.stopGrowth();
        }

        wordNumBits = getBits(wordDictionary.size() + 1);
        tagNumBits = getBits(tagDictionary.size() + 1);

        logger.info(String.format("[%d ms]%n", System.currentTimeMillis() - start));
        logger.info(String.format("Node feat bits: %d %n", numNodeFeatBits));
        logger.info(String.format("Lexical items: %d (%d bits)%n", wordDictionary.size(), wordNumBits));
        logger.info(String.format("Tag/label items: %d (%d bits)%n", tagDictionary.size(), tagNumBits));
    }

    public long[] createNodeFeats(DependencyInstance instance, int nodeId, double lambda) {
        TLongArrayList nodeFeats = new TLongArrayList();
        int i = nodeId;
        int W  = instance.formids[i];
        int P  = instance.postagids[i];
        int C  = instance.cpostagids[i];
        int L = instance.lemmaids == null ? 0 : instance.lemmaids[i];
        int pW = i > 0 ? instance.formids[i-1] : TOKEN_START;
        int pP = i > 0 ? instance.postagids[i-1] : TOKEN_START;
        int pC = i > 0 ? instance.cpostagids[i-1] : TOKEN_START;
        int nW = i < instance.formids.length-1 ? instance.formids[i+1] : TOKEN_END;
        int nP = i < instance.postagids.length-1 ? instance.postagids[i+1] : TOKEN_END;
        int nC = i < instance.cpostagids.length-1 ? instance.cpostagids[i+1] : TOKEN_END;

        nodeFeats.add(getCodeW(NodeFT.W, W));
        nodeFeats.add(getCodeW(NodeFT.pW, pW));
        nodeFeats.add(getCodeW(NodeFT.nW, nW));
        nodeFeats.add(getCodeP(NodeFT.P, P));
        nodeFeats.add(getCodeP(NodeFT.pP, pP));
        nodeFeats.add(getCodeP(NodeFT.nP, nP));
        nodeFeats.add(getCodeC(NodeFT.C, C));
        nodeFeats.add(getCodeP(NodeFT.pC, pC));
        nodeFeats.add(getCodeP(NodeFT.nC, nC));
        nodeFeats.add(getCodePP(NodeFT.pP_P, pP, P));
        nodeFeats.add(getCodePP(NodeFT.P_nP, P, nP));
        nodeFeats.add(getCodePW(NodeFT.P_W, P, W));

        int nonMorphFeats = nodeFeats.size();
        for(int j=0; j<MorphFeatTypes.INSTANCE.featsAlphabet.size(); j++) {
            nodeFeats.add(0);
            nodeFeats.add(0);
        }
        for (int j=0; j<instance.feats[nodeId].length; j++){
            if (!instance.feats[nodeId][j].equals("_") && !instance.feats[nodeId][j].equals("-")) {
                String morphFeatType = instance.feats[nodeId][j].split("=")[0].trim();
                assert(instance.feats[nodeId][j].split("=").length == 2);
                int featOffset = MorphFeatTypes.INSTANCE.featsAlphabet.lookupIndex(morphFeatType);
                long F = instance.featids[nodeId][j];
                nodeFeats.set(nonMorphFeats+(2*featOffset), getCodeF(NodeFT.F, F));
                nodeFeats.set(nonMorphFeats+(2*featOffset+1), getCodePF(NodeFT.P_F, P, F));
            }

        }

        return nodeFeats.toArray();

    }

    public long[] createEdgeFeats(DependencyInstance instance, int headId, int modifierId, double lambda) {
        TLongArrayList edgeFeats = new TLongArrayList();

        int lab = instance.deprelids[modifierId];
        int len = getBinnedDistance(Math.abs(headId - modifierId));
        int dir = headId > modifierId ? 0 : 1;
        edgeFeats.add(getCodeLab(EdgeFT.LAB, lab));
        edgeFeats.add(getCodeLenDir(EdgeFT.LEN_DIR, len, dir));
        return edgeFeats.toArray();

    }




    public void setNodeFeatVals(DependencyInstance instance) {
        int length = instance.forms.length;
        instance.nodeFeats = new long[length][];
        instance.edgeFeats = new long[length][];
        for (int i=0; i<length; ++i) {

            instance.nodeFeats[i] = createNodeFeats(instance, i, options.lambda);

        }

        for (int i=1; i<length; ++i) {

            instance.edgeFeats[i] = createEdgeFeats(instance, instance.heads[i], i, options.lambda);


        }
    }

    /************************************************************************
     * Region start #
     *
     *  Functions to create or parse 64-bit feature code
     *
     *  A feature code is like:
     *
     *    X1 X2 .. Xk TEMP DIST
     *
     *  where Xi   is the integer id of a word, pos tag, etc.
     *        TEMP is the integer id of the feature template
     *        DIST is the integer binned length  (4 bits)
     ************************************************************************/

    public int getBinnedDistance(int x) {
        int flag = 0;
        int add = 0;
        if (x < 0) {
            x = -x;
            //flag = 8;
            add = 7;
        }
        if (x > 10)          // x > 10
            flag |= 0x7;
        else if (x > 5)		 // x = 6 .. 10
            flag |= 0x6;
        else
            flag |= x;   	 // x = 1 .. 5
        return flag+add;
    }


    public long createArcCodeP(FeatureTemplate.Arc temp, long x) {
        return ((x << numArcFeatBits) | temp.ordinal()) << 4;
    }

    public long createArcCodePP(FeatureTemplate.Arc temp, long x, long y) {
        return ((((x << tagNumBits) | y) << numArcFeatBits) | temp.ordinal()) << 4;
    }

    public long createArcCodePPP(FeatureTemplate.Arc temp, long x, long y, long z) {
        return ((((((x << tagNumBits) | y) << tagNumBits) | z) << numArcFeatBits)
                | temp.ordinal()) << 4;
    }

    public long createArcCodePPPP(FeatureTemplate.Arc temp, long x, long y, long u, long v) {
        return ((((((((x << tagNumBits) | y) << tagNumBits) | u) << tagNumBits) | v)
                << numArcFeatBits) | temp.ordinal()) << 4;
    }

    public long createArcCodePPPPP(FeatureTemplate.Arc temp, long x, long y, long u, long v, long w) {
        return ((((((((((x << tagNumBits) | y) << tagNumBits) | u) << tagNumBits) | v) << tagNumBits) | w)
                << numArcFeatBits) | temp.ordinal()) << 4;
    }

    public long createArcCodeW(FeatureTemplate.Arc temp, long x) {
        return ((x << numArcFeatBits) | temp.ordinal()) << 4;
    }

    public long createArcCodeWW(FeatureTemplate.Arc temp, long x, long y) {
        return ((((x << wordNumBits) | y) << numArcFeatBits) | temp.ordinal()) << 4;
    }

    public long createArcCodeWP(FeatureTemplate.Arc temp, long x, long y) {
        return ((((x << tagNumBits) | y) << numArcFeatBits) | temp.ordinal()) << 4;
    }

    public long createArcCodeWPP(FeatureTemplate.Arc temp, long x, long y, long z) {
        return ((((((x << tagNumBits) | y) << tagNumBits) | z) << numArcFeatBits)
                | temp.ordinal()) << 4;
    }

    public long createArcCodeWPPP(FeatureTemplate.Arc temp, long x, long y, long u, long v) {
        return ((((((((x << tagNumBits) | y) << tagNumBits) | u) << tagNumBits) | v) << numArcFeatBits)
                | temp.ordinal()) << 4;
    }

    public long createArcCodeWWP(FeatureTemplate.Arc temp, long x, long y, long z) {
        return ((((((x << wordNumBits) | y) << tagNumBits) | z) << numArcFeatBits) | temp.ordinal()) << 4;
    }

    public long createArcCodeWWPP(FeatureTemplate.Arc temp, long x, long y, long u, long v) {
        return ((((((((x << wordNumBits) | y) << tagNumBits) | u) << tagNumBits) | v)
                << numArcFeatBits) | temp.ordinal()) << 4;
    }

    public long createWordCodeW(FeatureTemplate.Word temp, long x) {
        return ((x << numWordFeatBits) | temp.ordinal()) << 4;
    }

    public long createWordCodeP(FeatureTemplate.Word temp, long x) {
        return ((x << numWordFeatBits) | temp.ordinal()) << 4;
    }

    public long createWordCodePP(FeatureTemplate.Word temp, long x, long y) {
        return ((((x << tagNumBits) | y) << numWordFeatBits) | temp.ordinal()) << 4;
    }

    public long createWordCodePPP(FeatureTemplate.Word temp, long x, long y, long z) {
        return ((((((x << tagNumBits) | y) << tagNumBits) | z) << numWordFeatBits)
                | temp.ordinal()) << 4;
    }

    public long createWordCodeWP(FeatureTemplate.Word temp, long x, long y) {
        return ((((x << tagNumBits) | y) << numWordFeatBits) | temp.ordinal()) << 4;
    }

    /************************************************************************
     * Region start #
     *
     *  Functions that add feature codes into feature vectors and alphabets
     *
     ************************************************************************/

    public void addArcFeature(long code, FeatureVector fv) {
        int id = dataAlphabet.lookupIndex(code);
        if (id >= 0) {
            fv.add(id, 1.0);
            if (id == numArcFeats) ++numArcFeats;
        }
    }

    public void addArcFeature(long code, double value, FeatureVector fv) {
        int id = dataAlphabet.lookupIndex(code);
        if (id >= 0) {
            fv.add(id, value);
            if (id == numArcFeats) ++numArcFeats;
        }
    }



    public void addBasic1OFeatures(FeatureVector fv, DependencyInstance inst,
                                   int h, int m, int attDist)
    {
        //boolean notUsingKernel = options.kernelOrder == 0 ? true : false;
        long code = 0; 			// feature code

        int[] forms = inst.formids, lemmas = inst.lemmaids, postags = inst.postagids;
        int[] cpostags = inst.cpostagids;
        int[][] feats = inst.featids;


        code = createArcCodeW(CORE_HEAD_WORD, forms[h]);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodeW(CORE_MOD_WORD, forms[m]);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodeWW(HW_MW, forms[h], forms[m]);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        int pHF = h == 0 ? TOKEN_START : (h == m+1 ? TOKEN_MID : forms[h-1]);
        int nHF = h == inst.length() - 1 ? TOKEN_END : (h+1 == m ? TOKEN_MID : forms[h+1]);
        int pMF = m == 0 ? TOKEN_START : (m == h+1 ? TOKEN_MID : forms[m-1]);
        int nMF = m == inst.length() - 1 ? TOKEN_END : (m+1 == h ? TOKEN_MID : forms[m+1]);

        code = createArcCodeW(CORE_HEAD_pWORD, pHF);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodeW(CORE_HEAD_nWORD, nHF);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodeW(CORE_MOD_pWORD, pMF);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodeW(CORE_MOD_nWORD, nMF);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodeP(CORE_HEAD_POS, postags[h]);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodeP(CORE_HEAD_POS, cpostags[h]);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodeP(CORE_MOD_POS, postags[m]);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodeP(CORE_MOD_POS, cpostags[m]);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePP(HP_MP, postags[h], postags[m]);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePP(HP_MP, cpostags[h], cpostags[m]);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);



        if (lemmas != null) {
            code = createArcCodeW(CORE_HEAD_WORD, lemmas[h]);
            addArcFeature(code, fv);
            addArcFeature(code | attDist, fv);

            code = createArcCodeW(CORE_MOD_WORD, lemmas[m]);
            addArcFeature(code, fv);
            addArcFeature(code | attDist, fv);

            code = createArcCodeWW(HW_MW, lemmas[h], lemmas[m]);
            addArcFeature(code, fv);
            addArcFeature(code | attDist, fv);

            int pHL = h == 0 ? TOKEN_START : (h == m+1 ? TOKEN_MID : lemmas[h-1]);
            int nHL = h == inst.length() - 1 ? TOKEN_END : (h+1 == m ? TOKEN_MID : lemmas[h+1]);
            int pML = m == 0 ? TOKEN_START : (m == h+1 ? TOKEN_MID : lemmas[m-1]);
            int nML = m == inst.length() - 1 ? TOKEN_END : (m+1 == h ? TOKEN_MID : lemmas[m+1]);

            code = createArcCodeW(CORE_HEAD_pWORD, pHL);
            addArcFeature(code, fv);
            addArcFeature(code | attDist, fv);

            code = createArcCodeW(CORE_HEAD_nWORD, nHL);
            addArcFeature(code, fv);
            addArcFeature(code | attDist, fv);

            code = createArcCodeW(CORE_MOD_pWORD, pML);
            addArcFeature(code, fv);
            addArcFeature(code | attDist, fv);

            code = createArcCodeW(CORE_MOD_nWORD, nML);
            addArcFeature(code, fv);
            addArcFeature(code | attDist, fv);
        }

        if (feats[h] != null)
            for (int i = 0, N = feats[h].length; i < N; ++i) {
                code = createArcCodeP(CORE_HEAD_POS, feats[h][i]);
                addArcFeature(code, fv);
                addArcFeature(code | attDist, fv);
            }

        if (feats[m] != null)
            for (int i = 0, N = feats[m].length; i < N; ++i) {
                code = createArcCodeP(CORE_MOD_POS, feats[m][i]);
                addArcFeature(code, fv);
                addArcFeature(code | attDist, fv);
            }

        if (feats[h] != null && feats[m] != null) {
            for (int i = 0, N = feats[h].length; i < N; ++i)
                for (int j = 0, M = feats[m].length; j < M; ++j) {
                    code = createArcCodePP(HP_MP, feats[h][i], feats[m][j]);
                    addArcFeature(code, fv);
                    addArcFeature(code | attDist, fv);

                }
        }
        /*
        if (wordVectors != null) {

            int wvid = inst.wordVecIds[h];
            double [] v = wvid > 0 ? wordVectors[wvid] : unknownWv;
            if (v != null) {
                for (int i = 0; i < v.length; ++i) {
                    code = createArcCodeW(HEAD_EMB, i);
                    addArcFeature(code, v[i], fv);
                    addArcFeature(code | attDist, v[i], fv);
                }
            }

            wvid = inst.wordVecIds[m];
            v = wvid > 0 ? wordVectors[wvid] : unknownWv;
            if (v != null) {
                for (int i = 0; i < v.length; ++i) {
                    code = createArcCodeW(MOD_EMB, i);
                    addArcFeature(code, v[i], fv);
                    addArcFeature(code | attDist, v[i], fv);
                }
            }
        }
        */
    }

    public void addCore1OPosFeatures(FeatureVector fv, DependencyInstance inst,
                                     int h, int c, int attDist)
    {
        //boolean notUsingKernel = options.kernelOrder == 0 ? true : false;
        int[] pos = inst.postagids;
        int[] posA = inst.cpostagids;

        int pHead = pos[h], pHeadA = posA[h];
        int pMod = pos[c], pModA = posA[c];
        int pHeadLeft = h > 0 ? (h-1 == c ? TOKEN_MID : pos[h-1]) : TOKEN_START;
        int pModRight = c < pos.length-1 ? (c+1 == h ? TOKEN_MID : pos[c+1]) : TOKEN_END;
        int pHeadRight = h < pos.length-1 ? (h+1 == c ? TOKEN_MID: pos[h+1]) : TOKEN_END;
        int pModLeft = c > 0 ? (c-1 == h ? TOKEN_MID : pos[c-1]) : TOKEN_START;
        int pHeadLeftA = h > 0 ? (h-1 == c ? TOKEN_MID : posA[h-1]) : TOKEN_START;
        int pModRightA = c < posA.length-1 ? (c+1 == h ? TOKEN_MID : posA[c+1]) : TOKEN_END;
        int pHeadRightA = h < posA.length-1 ? (h+1 == c ? TOKEN_MID: posA[h+1]) : TOKEN_END;
        int pModLeftA = c > 0 ? (c-1 == h ? TOKEN_MID : posA[c-1]) : TOKEN_START;


        long code = 0;

        // feature posR posMid posL
        int small = h < c ? h : c;
        int large = h > c ? h : c;
        for(int i = small+1; i < large; i++) {
            code = createArcCodePPP(HP_BP_MP, pHead, pos[i], pMod);
            addArcFeature(code, fv);
            addArcFeature(code | attDist, fv);

            code = createArcCodePPP(HP_BP_MP, pHeadA, posA[i], pModA);
            addArcFeature(code, fv);
            addArcFeature(code | attDist, fv);
        }

        // feature posL-1 posL posR posR+1
        code = createArcCodePPPP(HPp_HP_MP_MPn, pHeadLeft, pHead, pMod, pModRight);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePPP(HP_MP_MPn, pHead, pMod, pModRight);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePPP(HPp_HP_MP, pHeadLeft, pHead, pMod);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePPP(HPp_MP_MPn, pHeadLeft, pMod, pModRight);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePPP(HPp_HP_MPn, pHeadLeft, pHead, pModRight);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePPPP(HPp_HP_MP_MPn, pHeadLeftA, pHeadA, pModA, pModRightA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPP(HP_MP_MPn, pHeadA, pModA, pModRightA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPP(HPp_HP_MP, pHeadLeftA, pHeadA, pModA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPP(HPp_MP_MPn, pHeadLeftA, pModA, pModRightA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPP(HPp_HP_MPn, pHeadLeftA, pHeadA, pModRightA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);



        // feature posL posL+1 posR-1 posR
        code = createArcCodePPPP(HP_HPn_MPp_MP, pHead, pHeadRight, pModLeft, pMod);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePPP(HP_MPp_MP, pHead, pModLeft, pMod);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePPP(HP_HPn_MP, pHead, pHeadRight, pMod);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePPP(HPn_MPp_MP, pHeadRight, pModLeft, pMod);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePPP(HP_HPn_MPp, pHead, pHeadRight, pModLeft);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodePPPP(HP_HPn_MPp_MP, pHeadA, pHeadRightA, pModLeftA, pModA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPP(HP_MPp_MP, pHeadA, pModLeftA, pModA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPP(HP_HPn_MP, pHeadA, pHeadRightA, pModA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPP(HPn_MPp_MP, pHeadRightA, pModLeftA, pModA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPP(HP_HPn_MPp, pHeadA, pHeadRightA, pModLeftA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);



        // feature posL-1 posL posR-1 posR
        // feature posL posL+1 posR posR+1
        code = createArcCodePPPP(HPp_HP_MPp_MP, pHeadLeft, pHead, pModLeft, pMod);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPPP(HP_HPn_MP_MPn, pHead, pHeadRight, pMod, pModRight);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPPP(HPp_HP_MPp_MP, pHeadLeftA, pHeadA, pModLeftA, pModA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodePPPP(HP_HPn_MP_MPn, pHeadA, pHeadRightA, pModA, pModRightA);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


    }

    public void addCore1OBigramFeatures(FeatureVector fv, int head, int headP,
                                        int mod, int modP, int attDist)
    {

        long code = 0;

        code = createArcCodeWWPP(HW_MW_HP_MP, head, mod, headP, modP);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);



        code = createArcCodeWPP(MW_HP_MP, mod, headP, modP);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodeWPP(HW_HP_MP, head, headP, modP);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodeWP(MW_HP, mod, headP);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);

        code = createArcCodeWP(HW_MP, head, modP);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodeWP(HW_HP, head, headP);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


        code = createArcCodeWP(MW_MP, mod, modP);
        addArcFeature(code, fv);
        addArcFeature(code | attDist, fv);


    }


    /**
     * Create 1st order feature vector of an dependency arc.
     *
     * This is an re-implementation of MST parser 1st order feature construction.
     * There is slightly difference on constructions of morphology features and
     * bigram features, in order to reduce redundancy.
     *
     * @param inst 	the current sentence
     * @param h		index of the head
     * @param c		index of the modifier
     * @return
     */
    public void addArcFeatures(DependencyInstance inst, int h, int c, FeatureVector fv)
    {

        int attDist = getBinnedDistance(h-c);

        addBasic1OFeatures(fv, inst, h, c, attDist);

        addCore1OPosFeatures(fv, inst, h, c, attDist);

        addCore1OBigramFeatures(fv, inst.formids[h], inst.postagids[h],
                inst.formids[c], inst.postagids[c], attDist);

        if (inst.lemmaids != null)
            addCore1OBigramFeatures(fv, inst.lemmaids[h], inst.postagids[h],
                    inst.lemmaids[c], inst.postagids[c], attDist);

        addCore1OBigramFeatures(fv, inst.formids[h], inst.cpostagids[h],
                inst.formids[c], inst.cpostagids[c], attDist);

        if (inst.lemmaids != null)
            addCore1OBigramFeatures(fv, inst.lemmaids[h], inst.cpostagids[h],
                    inst.lemmaids[c], inst.cpostagids[c], attDist);

        if (inst.featids[h] != null && inst.featids[c] != null) {
            for (int i = 0, N = inst.featids[h].length; i < N; ++i)
                for (int j = 0, M = inst.featids[c].length; j < M; ++j) {

                    addCore1OBigramFeatures(fv, inst.formids[h], inst.featids[h][i],
                            inst.formids[c], inst.featids[c][j], attDist);

                    if (inst.lemmas != null)
                        addCore1OBigramFeatures(fv, inst.lemmaids[h], inst.featids[h][i],
                                inst.lemmaids[c], inst.featids[c][j], attDist);
                }
        }

    }

    public void addLabelFeatures(DependencyInstance inst, int word,
                                          int type, boolean toRight, boolean isChild, FeatureVector fv)
    {

        int att = 1;
        if (toRight) att |= 2;
        if (isChild) att |= 4;

        int[] toks = inst.formids;
        int[] pos = inst.postagids;

        int w = toks[word];

        long code = 0;

        code = createArcCodeP(CORE_LABEL_NTS1, type);
        addArcFeature(code, fv);
        addArcFeature(code | att, fv);

        int wP = pos[word];
        int wPm1 = word > 0 ? pos[word-1] : TOKEN_START;
        int wPp1 = word < pos.length-1 ? pos[word+1] : TOKEN_END;

        code = createArcCodeWPP(CORE_LABEL_NTH, w, wP, type);
        addArcFeature(code, fv);
        addArcFeature(code | att, fv);

        code = createArcCodePP(CORE_LABEL_NTI, wP, type);
        addArcFeature(code, fv);
        addArcFeature(code | att, fv);

        code = createArcCodePPP(CORE_LABEL_NTIA, wPm1, wP, type);
        addArcFeature(code, fv);
        addArcFeature(code | att, fv);

        code = createArcCodePPP(CORE_LABEL_NTIB, wP, wPp1, type);
        addArcFeature(code, fv);
        addArcFeature(code | att, fv);

        code = createArcCodePPPP(CORE_LABEL_NTIC, wPm1, wP, wPp1, type);
        addArcFeature(code, fv);
        addArcFeature(code | att, fv);

        code = createArcCodeWP(CORE_LABEL_NTJ, w, type);
        addArcFeature(code, fv);
        addArcFeature(code | att, fv);
    }



    /**
     * Creates the data alphabet, the type alphabet was already created when we created
     * the datasets in {@link #createDictionaries}.
     * created the dataset.
     * @param trainset
     * @throws IOException
     */
    public void createAlphabet(Dataset trainset) throws IOException {


        logger.info("Creating Alphabet ...\n");
        for (int i=0; i<trainset.size(); ++i) {
            if (i%1000==0) {
                logger.info(String.format("%d ", i));
            }
            List<DependencyInstance> sentence = trainset.get(i).getTrees();
            // Create features for the gold tree plus options.rerankerPruneK
            // trees from the k-best list.
            createFeatureVector(sentence.get(0));
            for (int j=1; j<sentence.size(); ++j) {
                if (j>options.rerankerPruneK) {
                    break;
                }
                createFeatureVector(sentence.get(j));
            }
        }
        closeAlphabets();
        logger.info("Done.\n");
        int numFeats = dataAlphabet.size();
        logger.info(String.format("Num Feats: %d\n", numFeats));


    }

    public void closeAlphabets() {
        dataAlphabet.stopGrowth();
        typeAlphabet.stopGrowth();
    }


    public FeatureVector createFeatureVector(DependencyInstance inst)  {
        int n = inst.length();
        int[] heads = inst.heads;
        int[] deplbids = inst.deplbids;

        FeatureVector fv = new FeatureVector();
        if (options.baseFeatures >= 1) {
            // 1st order arc
            for (int i = 0; i < n; ++i) {

                if (heads[i] == -1) continue;

                int parent = heads[i];
                addArcFeatures(inst, parent, i, fv);    // arc features

                int type = deplbids[i];
                boolean toRight = parent < i;
                addLabelFeatures(inst, parent, type, toRight, false, fv);
                addLabelFeatures(inst, i, type, toRight, true, fv);

            }
        }
        if (options.baseFeatures >= 2) {
            DependencyArcList arcLis = new DependencyArcList(heads);
            // 2nd order (h,m,s) & (m,s)
            for (int h = 0; h < n; ++h) {

                int st = arcLis.startIndex(h);
                int ed = arcLis.endIndex(h);

                for (int p = st; p + 1 < ed; ++p) {
                    // mod and sib
                    int m = arcLis.get(p);
                    int s = arcLis.get(p + 1);

                    // consecutive siblings
                    addTripsFeatureVector(inst, h, m, s, fv);
                    addSibFeatureVector(inst, m, s, fv);


                    // gp-sibling
                    int gp = heads[h];
                    if (gp >= 0) {
                        addGPSibFeatureVector(inst, gp, h, m, s, fv);
                    }

                    // tri-sibling
                    if (p + 2 < ed) {
                        int s2 = arcLis.get(p + 2);
                        addTriSibFeatureVector(inst, h, m, s, s2, fv);
                    }

                    // parent, sibling and child

                    // mod's child
                    int mst = arcLis.startIndex(m);
                    int med = arcLis.endIndex(m);

                    for (int mp = mst; mp < med; ++mp) {
                        int c = arcLis.get(mp);
                        addPSCFeatureVector(inst, h, m, c, s, fv);
                    }

                    // sib's child
                    int sst = arcLis.startIndex(s);
                    int sed = arcLis.endIndex(s);

                    for (int sp = sst; sp < sed; ++sp) {
                        int c = arcLis.get(sp);
                        addPSCFeatureVector(inst, h, s, c, m, fv);
                    }

                }
            }


            for (int m = 1; m < n; ++m) {
                int h = heads[m];

                Utils.Assert(h >= 0);

                // grandparent
                int gp = heads[h];
                if (gp != -1) {
                    addGPCFeatureVector(inst, gp, h, m, fv);
                }

                // head bigram
                if ( m + 1 < n) {
                    int h2 = heads[m + 1];
                    Utils.Assert(h2 >= 0);

                    addHeadBiFeatureVector(inst, m, h, h2, fv);
                }

                // great-grandparent
                if (gp != -1 && heads[gp] != -1) {
                    int ggp = heads[gp];
                    addGGPCFeatureVector(inst, ggp, gp, h, m, fv);
                }
            }

        }


        return fv;
    }

    public void addTripsFeatureVector(DependencyInstance inst, int par,
                                                  int ch1, int ch2, FeatureVector fv) {



        int[] pos = inst.postagids;
        int[] posA = inst.cpostagids;

        // ch1 is always the closes to par
        int dirFlag = ((((par < ch1 ? 0 : 1) << 1) | (par < ch2 ? 0 : 1)) << 1) | 1;

        int HP = pos[par];
        int SP = ch1 == par ? TOKEN_START : pos[ch1];
        int MP = pos[ch2];
        int HC = posA[par];
        int SC = ch1 == par ? TOKEN_START : posA[ch1];
        int MC  = posA[ch2];

        long code = 0;

        code = createArcCodePPP(HP_SP_MP, HP, SP, MP);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPP(HC_SC_MC, HC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        addTurboSib(inst, par, ch1, ch2, dirFlag, fv);


    }

    void addTurboSib(DependencyInstance inst, int par, int ch1, int ch2, int dirFlag, FeatureVector fv)
    {
        int[] posA = inst.cpostagids;
        //int[] lemma = inst.lemmaids;
        int[] lemma = inst.lemmaids != null ? inst.lemmaids : inst.formids;
        int len = inst.length();

        int HC = posA[par];
        int SC = ch1 == par ? TOKEN_START : posA[ch1];
        int MC = posA[ch2];

        int pHC = par > 0 ? posA[par - 1] : TOKEN_START;
        int nHC = par < len - 1 ? posA[par + 1] : TOKEN_END;
        int pSC = ch1 > 0 ? posA[ch1 - 1] : TOKEN_START;
        int nSC = ch1 < len - 1 ? posA[ch1 + 1] : TOKEN_END;
        int pMC = ch2 > 0 ? posA[ch2 - 1] : TOKEN_START;
        int nMC = ch2 < len - 1 ? posA[ch2 + 1] : TOKEN_END;

        long code = 0;

        // CCC
        code = createArcCodePPPP(pHC_HC_SC_MC, pHC, HC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(HC_nHC_SC_MC, HC, nHC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(HC_pSC_SC_MC, HC, pSC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(HC_SC_nSC_MC, HC, SC, nSC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(HC_SC_pMC_MC, HC, SC, pMC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(HC_SC_MC_nMC, HC, SC, MC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        int HL = lemma[par];
        int SL = ch1 == par ? TOKEN_START : lemma[ch1];
        int ML = lemma[ch2];

        // LCC
        code = createArcCodeWPPP(pHC_HL_SC_MC, HL, pHC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HL_nHC_SC_MC, HL, nHC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HL_pSC_SC_MC, HL, pSC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HL_SC_nSC_MC, HL, SC, nSC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HL_SC_pMC_MC, HL, SC, pMC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HL_SC_MC_nMC, HL, SC, MC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        // CLC
        code = createArcCodeWPPP(pHC_HC_SL_MC, SL, pHC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HC_nHC_SL_MC, SL, HC, nHC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HC_pSC_SL_MC, SL, HC, pSC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HC_SL_nSC_MC, SL, HC, nSC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HC_SL_pMC_MC, SL, HC, pMC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HC_SL_MC_nMC, SL, HC, MC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        // CCL
        code = createArcCodeWPPP(pHC_HC_SC_ML, ML, pHC, HC, SC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HC_nHC_SC_ML, ML, HC, nHC, SC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HC_pSC_SC_ML, ML, HC, pSC, SC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HC_SC_nSC_ML, ML, HC, SC, nSC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HC_SC_pMC_ML, ML, HC, SC, pMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(HC_SC_ML_nMC, ML, HC, SC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_pHC_pMC, HC, MC, SC, pHC, pMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_pHC_pSC, HC, MC, SC, pHC, pSC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_pMC_pSC, HC, MC, SC, pMC, pSC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_nHC_nMC, HC, MC, SC, nHC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_nHC_nSC, HC, MC, SC, nHC, nSC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_nMC_nSC, HC, MC, SC, nMC, nSC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_pHC_nMC, HC, MC, SC, pHC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_pHC_nSC, HC, MC, SC, pHC, nSC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_pMC_nSC, HC, MC, SC, pMC, nSC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_nHC_pMC, HC, MC, SC, nHC, pMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_nHC_pSC, HC, MC, SC, nHC, pSC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(HC_MC_SC_nMC_pSC, HC, MC, SC, nMC, pSC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);
    }

    public void addSibFeatureVector(DependencyInstance inst, int ch1, int ch2/*, boolean isST*/, FeatureVector fv) {



        int[] pos = inst.postagids;
        int[] posA = inst.cpostagids;
        int[] toks = inst.formids;
        //int[] lemma = inst.lemmaids;
        int[] lemma = inst.lemmaids != null ? inst.lemmaids : inst.formids;

        // ch1 is always the closes to par

        int SP = /*isST ? TOKEN_START :*/ pos[ch1];
        int MP = pos[ch2];
        int SW = /*isST ? TOKEN_START :*/ toks[ch1];
        int MW = toks[ch2];
        int SC = /*isST ? TOKEN_START :*/ posA[ch1];
        int MC = posA[ch2];


        //Utils.Assert(ch1 < ch2);
        int flag = getBinnedDistance(ch1 - ch2);

        long code = 0;

        code = createArcCodePP(SP_MP, SP, MP);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWW(SW_MW, SW, MW);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWP(SW_MP, SW, MP);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWP(SP_MW, MW, SP);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodePP(SC_MC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);


        int SL = /*isST ? TOKEN_START :*/ lemma[ch1];
        int ML = lemma[ch2];

        code = createArcCodeWW(SL_ML, SL, ML);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWP(SL_MC, SL, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWP(SC_ML, ML, SC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);



    }

    public void addHeadBiFeatureVector(DependencyInstance inst, int ch, int par1, int par2, FeatureVector fv) {


        int[] pos = inst.postagids;
        int[] posA = inst.cpostagids;

        // exactly 16 combination, so need one more template
        int flag = 0;
        if (par1 == par2)
            flag = 1;
        else if (par1 == ch + 1)
            flag = 2;
        else if (par2 == ch)
            flag = 3;

        int dirFlag = flag;
        dirFlag = (dirFlag << 1) | (par1 < ch ? 1 : 0);
        dirFlag = (dirFlag << 1) | (par2 < ch + 1 ? 1 : 0);

        long code = 0;

        int H1P = pos[par1];
        int H2P = pos[par2];
        int M1P = pos[ch];
        int M2P = pos[ch + 1];
        int H1C = posA[par1];
        int H2C = posA[par2];
        int M1C = posA[ch];
        int M2C = posA[ch + 1];

        code = createArcCodePPPP(H1P_H2P_M1P_M2P, H1P, H2P, M1P, M2P);
        addArcFeature(code | flag, fv);
        code = createArcCodePPPP(H1P_H2P_M1P_M2P_DIR, H1P, H2P, M1P, M2P);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(H1C_H2C_M1C_M2C, H1C, H2C, M1C, M2C);
        addArcFeature(code | flag, fv);
        code = createArcCodePPPP(H1C_H2C_M1C_M2C_DIR, H1C, H2C, M1C, M2C);
        addArcFeature(code | dirFlag, fv);


    }

    public void addGPCFeatureVector(DependencyInstance inst, int gp, int par, int c, FeatureVector fv) {



        int[] pos = inst.postagids;
        int[] posA = inst.cpostagids;
        //int[] lemma = inst.lemmaids;
        int[] lemma = inst.lemmaids != null ? inst.lemmaids : inst.formids;

        int flag = (((((gp < par ? 0 : 1) << 1) | (par < c ? 0 : 1)) << 1) | 1);

        int GP = pos[gp];
        int HP = pos[par];
        int MP = pos[c];
        int GC = posA[gp];
        int HC = posA[par];
        int MC = posA[c];
        long code = 0;

        code = createArcCodePPP(GP_HP_MP, GP, HP, MP);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodePPP(GC_HC_MC, GC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        int GL = lemma[gp];
        int HL = lemma[par];
        int ML = lemma[c];

        code = createArcCodeWPP(GL_HC_MC, GL, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(GC_HL_MC, HL, GC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(GC_HC_ML, ML, GC, HC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        addTurboGPC(inst, gp, par, c, flag, fv);


    }

    void addTurboGPC(DependencyInstance inst, int gp, int par, int c, int dirFlag, FeatureVector fv) {
        int[] posA = inst.cpostagids;
        //int[] lemma = inst.lemmaids;
        int[] lemma = inst.lemmaids != null ? inst.lemmaids : inst.formids;
        int len = posA.length;

        int GC = posA[gp];
        int HC = posA[par];
        int MC = posA[c];

        int pGC = gp > 0 ? posA[gp - 1] : TOKEN_START;
        int nGC = gp < len - 1 ? posA[gp + 1] : TOKEN_END;
        int pHC = par > 0 ? posA[par - 1] : TOKEN_START;
        int nHC = par < len - 1 ? posA[par + 1] : TOKEN_END;
        int pMC = c > 0 ? posA[c - 1] : TOKEN_START;
        int nMC = c < len - 1 ? posA[c + 1] : TOKEN_END;

        long code = 0;

        // CCC
        code = createArcCodePPPP(pGC_GC_HC_MC, pGC, GC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(GC_nGC_HC_MC, GC, nGC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(GC_pHC_HC_MC, GC, pHC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(GC_HC_nHC_MC, GC, HC, nHC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(GC_HC_pMC_MC, GC, HC, pMC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPP(GC_HC_MC_nMC, GC, HC, MC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_pGC_pHC, GC, HC, MC, pGC, pHC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_pGC_pMC, GC, HC, MC , pGC, pMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_pHC_pMC, GC, HC, MC, pHC, pMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_nGC_nHC, GC, HC, MC, nGC, nHC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_nGC_nMC, GC, HC, MC, nGC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_nHC_nMC, GC, HC, MC, nHC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_pGC_nHC, GC, HC, MC, pGC, nHC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_pGC_nMC, GC, HC, MC, pGC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_pHC_nMC, GC, HC, MC, pHC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_nGC_pHC, GC, HC, MC, nGC, pHC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_nGC_pMC, GC, HC, MC, nGC, pMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePPPPP(GC_HC_MC_nHC_pMC, GC, HC, MC, nHC, pMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePP(GC_HC, GC, HC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePP(GC_MC, GC, MC);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodePP(HC_MC, HC, MC);
        addArcFeature(code | dirFlag, fv);

        int GL = lemma[gp];
        int HL = lemma[par];
        int ML = lemma[c];

        // LCC
        code = createArcCodeWPPP(pGC_GL_HC_MC, GL, pGC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GL_nGC_HC_MC, GL, nGC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GL_pHC_HC_MC, GL, pHC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GL_HC_nHC_MC, GL, HC, nHC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GL_HC_pMC_MC, GL, HC, pMC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GL_HC_MC_nMC, GL, HC, MC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        // CLC
        code = createArcCodeWPPP(pGC_GC_HL_MC, HL, pGC, GC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GC_nGC_HL_MC, HL, GC, nGC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GC_pHC_HL_MC, HL, GC, pHC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GC_HL_nHC_MC, HL, GC, nHC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GC_HL_pMC_MC, HL, GC, pMC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GC_HL_MC_nMC, HL, GC, MC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        // CCL
        code = createArcCodeWPPP(pGC_GC_HC_ML, ML, pGC, GC, HC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GC_nGC_HC_ML, ML, GC, nGC, HC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GC_pHC_HC_ML, ML, GC, pHC, HC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GC_HC_nHC_ML, ML, GC, HC, nHC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GC_HC_pMC_ML, ML, GC, HC, pMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWPPP(GC_HC_ML_nMC, ML, GC, HC, nMC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWWP(GL_HL_MC, GL, HL, MC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWWP(GL_HC_ML, GL, ML, HC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWWP(GC_HL_ML, HL, ML, GC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        //code = createArcCodeWWW(GL_HL_ML, GL, HL, ML);
        //addCode(TemplateType::TThirdOrder, code, fv);

        code = createArcCodeWP(GL_HC, GL, HC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWP(GC_HL, HL, GC);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWW(GL_HL, GL, HL);
        addArcFeature(code, fv);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWP(GL_MC, GL, MC);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWP(GC_ML, ML, GC);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWW(GL_ML, GL, ML);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWP(HL_MC, HL, MC);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWP(HC_ML, ML, HC);
        addArcFeature(code | dirFlag, fv);

        code = createArcCodeWW(HL_ML, HL, ML);
        addArcFeature(code | dirFlag, fv);
    }

    /************************************************************************
     *  Region end #
     ************************************************************************/


    /************************************************************************
     * Region start #
     *
     *  Functions that create 3rd order feature vectors
     *
     ************************************************************************/

    public void addGPSibFeatureVector(DependencyInstance inst, int par, int arg, int prev, int curr, FeatureVector fv) {


        int[] posA = inst.cpostagids;
        //int[] lemma = inst.lemmaids;
        int[] lemma = inst.lemmaids != null ? inst.lemmaids : inst.formids;

        int flag = par < arg ? 0 : 1;					// bit 1
        flag = (flag << 1) | (arg < prev ? 0 : 1);		// bit 2
        flag = (flag << 1) | (arg < curr ? 0 : 1);		// bit 3
        flag = (flag << 1) | 1;							// bit 4

        int GC = posA[par];
        int HC = posA[arg];
        int SC = posA[prev];
        int MC = posA[curr];
        long code = 0;

        code = createArcCodePPPP(GC_HC_MC_SC, GC, HC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        int GL = lemma[par];
        int HL = lemma[arg];
        int SL = lemma[prev];
        int ML = lemma[curr];

        code = createArcCodeWPPP(GL_HC_MC_SC, GL, HC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPPP(GC_HL_MC_SC, HL, GC, SC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPPP(GC_HC_ML_SC, ML, GC, HC, SC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPPP(GC_HC_MC_SL, SL, GC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);


    }

    public void addTriSibFeatureVector(DependencyInstance inst, int arg, int prev, int curr, int next, FeatureVector fv) {


        int[] posA = inst.cpostagids;
        //int[] lemma = inst.lemmaids;
        int[] lemma = inst.lemmaids != null ? inst.lemmaids : inst.formids;

        int flag = arg < prev ? 0 : 1;					// bit 1
        flag = (flag << 1) | (arg < curr ? 0 : 1);		// bit 2
        flag = (flag << 1) | (arg < next ? 0 : 1);		// bit 3
        flag = (flag << 1) | 1;							// bit 4

        int HC = posA[arg];
        int PC = posA[prev];
        int MC = posA[curr];
        int NC = posA[next];
        long code = 0;

        code = createArcCodePPPP(HC_PC_MC_NC, HC, PC, MC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodePPP(HC_PC_NC, HC, PC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodePPP(PC_MC_NC, PC, MC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodePP(PC_NC, PC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        int HL = lemma[arg];
        int PL = lemma[prev];
        int ML = lemma[curr];
        int NL = lemma[next];

        code = createArcCodeWPPP(HL_PC_MC_NC, HL, PC, MC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPPP(HC_PL_MC_NC, PL, HC, MC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPPP(HC_PC_ML_NC, ML, HC, PC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPPP(HC_PC_MC_NL, NL, HC, PC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(HL_PC_NC, HL, PC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(HC_PL_NC, PL, HC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(HC_PC_NL, NL, HC, PC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(PL_MC_NC, PL, MC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(PC_ML_NC, ML, PC, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(PC_MC_NL, NL, PC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWP(PL_NC, PL, NC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWP(PC_NL, NL, PC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);


    }

    public void addGGPCFeatureVector(DependencyInstance inst, int ggp, int gp, int par, int c, FeatureVector fv) {


        int[] posA = inst.cpostagids;
        int[] lemma = inst.lemmaids != null ? inst.lemmaids : inst.formids;

        int flag = ggp < gp ? 0 : 1;				// bit 4
        flag = (flag << 1) | (gp < par ? 0 : 1);	// bit 3
        flag = (flag << 1) | (par < c ? 0 : 1);		// bit 2
        flag = (flag << 1) | 1;						// bit 1

        int GGC = posA[ggp];
        int GC = posA[gp];
        int HC = posA[par];
        int MC = posA[c];
        int GGL = lemma[ggp];
        int GL = lemma[gp];
        int HL = lemma[par];
        int ML = lemma[c];

        long code = 0;

        code = createArcCodePPPP(GGC_GC_HC_MC, GGC, GC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPPP(GGL_GC_HC_MC, GGL, GC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPPP(GGC_GL_HC_MC, GL, GGC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPPP(GGC_GC_HL_MC, HL, GGC, GC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPPP(GGC_GC_HC_ML, ML, GGC, GC, HC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodePPP(GGC_HC_MC, GGC, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(GGL_HC_MC, GGL, HC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(GGC_HL_MC, HL, GGC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(GGC_HC_ML, ML, GGC, HC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodePPP(GGC_GC_MC, GGC, GC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(GGL_GC_MC, GGL, GC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(GGC_GL_MC, GL, GGC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWPP(GGC_GC_ML, ML, GGC, GC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodePP(GGC_MC, GGC, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWP(GGL_MC, GGL, MC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWP(GGC_ML, ML, GGC);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);

        code = createArcCodeWW(GGL_ML, GGL, ML);
        addArcFeature(code, fv);
        addArcFeature(code | flag, fv);


    }

    public void addPSCFeatureVector(DependencyInstance inst, int par, int mod, int ch, int sib, FeatureVector fv) {


        int[] posA = inst.cpostagids;
        int[] lemma = inst.lemmaids != null ? inst.lemmaids : inst.formids;

        int type = (mod - par) * (mod - sib) > 0 ? 0x0 : 0x1;	// 0-1

        int dir = 0x0;						// 0-2
        if (mod < par && sib < par)
            dir = 0x0;
        else if (mod > par && sib > par)
            dir = 0x1;
        else
            dir = 0x2;

        dir = (dir << 1) | (mod < ch ? 0x0 : 0x1);	// 0-5
        dir = (dir << 1) | type;		// 0-11
        dir += 2;			// 2-13

        int HC = posA[par];
        int MC = posA[mod];
        int CC = posA[ch];
        int SC = posA[sib];
        int HL = lemma[par];
        int ML = lemma[mod];
        int CL = lemma[ch];
        int SL = lemma[sib];

        long code = 0;

        code = createArcCodePPPP(HC_MC_CC_SC, HC, MC, CC, SC);
        addArcFeature(code | type, fv);
        addArcFeature(code | dir, fv);

        code = createArcCodeWPPP(HL_MC_CC_SC, HL, MC, CC, SC);
        addArcFeature(code | type, fv);
        addArcFeature(code | dir, fv);

        code = createArcCodeWPPP(HC_ML_CC_SC, ML, HC, CC, SC);
        addArcFeature(code | type, fv);
        addArcFeature(code | dir, fv);

        code = createArcCodeWPPP(HC_MC_CL_SC, CL, HC, MC, SC);
        addArcFeature(code | type, fv);
        addArcFeature(code | dir, fv);

        code = createArcCodeWPPP(HC_MC_CC_SL, SL, HC, MC, CC);
        addArcFeature(code | type, fv);
        addArcFeature(code | dir, fv);

        code = createArcCodePPP(HC_CC_SC, HC, CC, SC);
        addArcFeature(code | type, fv);
        addArcFeature(code | dir, fv);

        code = createArcCodeWPP(HL_CC_SC, HL, CC, SC);
        addArcFeature(code | type, fv);
        addArcFeature(code | dir, fv);

        code = createArcCodeWPP(HC_CL_SC, CL, HC, SC);
        addArcFeature(code | type, fv);
        addArcFeature(code | dir, fv);

        code = createArcCodeWPP(HC_CC_SL, SL, HC, CC);
        addArcFeature(code | type, fv);
        addArcFeature(code | dir, fv);


    }


    /************************************************************************
     *  Region end #
     ************************************************************************/

    public Dictionary getTagDictionary() {
        return tagDictionary;
    }

    
    public Dictionary getWordDictionary() {
        return wordDictionary;
    }
}

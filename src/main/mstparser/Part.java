package mstparser;




/**
 * Created by hillel on 10/2/14.
 */
public interface Part {

    public enum PartType{
        ARC_PART,
        CONSECUTIVE_SIBLINGS_PART
    }

    public PartType type();



}

package mstparser;

/**
 * Created by hillel on 7/26/14.
 */
public class KernelFeatureTemplate {

    /******************************
     * template type
     *****************************/

    public enum TemplateType {
        TNodeFT, TEdgeFT
    }

    public static int largeOff = 17;	// word, lemma
    public static int midOff = 9; 		// pos, cpos, type



    /**********************************
     * notation
     * P: pos; C: coarse pos; W: word; L: lemma
     **********************************/
    public enum NodeFT {
        START,
        W,
        P,
        C,
        F,
        pW,
        pP,
        pC,
        nW,
        nP,
        nC,
        pC_C,
        C_nC,
        pP_P,
        P_nP,
        P_W,
        pP_pW,
        nP_nW,
        P_F,
        pP_P_nP,
        COUNT;

        static int numNodeFeatBits = getBits(COUNT.ordinal());
    }

    /**********************************
     * notation
     * LAB: edge label; LEN: h-m distance; DIR: edge direction
     **********************************/
    public enum EdgeFT {
        START,
        LAB,
        inP,
        LAB_inP,
        LEN,
        DIR,
        LEN_DIR,
        LAB_DIR,
        LAB_LEN_DIR,
        COUNT;

        static int numEdgeFeatBits = getBits(COUNT.ordinal());
    }

    public static int getBits(long x) {
        long y = 1;
        int i = 0;
        while (y < x) {
            y = y << 1;
            ++i;
        }
        return i;
    }






}

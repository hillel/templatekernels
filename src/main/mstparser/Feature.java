///////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2007 University of Texas at Austin and (C) 2005
// University of Pennsylvania and Copyright (C) 2002, 2003 University
// of Massachusetts Amherst, Department of Computer Science.
//
// This software is licensed under the terms of the Common Public
// License, Version 1.0 or (at your option) any subsequent version.
// 
// The license is approved by the Open Source Initiative, and is
// available from their website at http://www.opensource.org.
///////////////////////////////////////////////////////////////////////////////

package mstparser;

import gnu.trove.*;
import gnu.trove.list.TLinkable;

import java.text.DecimalFormat;
import java.util.Map;


/**
 * A simple class holding a feature index and value that can be used
 * in a TLinkedList.
 *
 * <p>
 * Created: Sat Nov 10 15:25:10 2001
 * </p>
 *
 * @author Jason Baldridge
 * @version $Id: TLinkedList.java,v 1.5 2005/03/26 17:52:56 ericdf Exp $
 * @see mstparser.FeatureVector
 */

public final class Feature implements TLinkable {

    public int index;
    public double value;

    private Feature next = null;
    private Feature prev = null;

    public Feature (int i, double v) {
	index = i;
	value = v;
    }

    public final Feature clone () {
	return new Feature(index, value);
    }

    public final Feature negation () {
	return new Feature(index, -value);
    }

    public final String toString() {
	return index+"="+value;
    }

    public final String toString(String sep) {
        return index+sep+value;
    }
    public final String toString(String sep, double[] params) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        String weight = df.format(params[index]);
        weight = weight.replaceAll(",", "");
        return index+sep+value+sep+weight;
    }


    public final String toString(Map<Integer, String> transMap) {
        String feat = transMap.get(index);
        if (feat != null) {
            return feat + "=" + value;
        } else {
            return index+"="+value;
        }

    }

    @Override
    public TLinkable getNext() {
        return next;
    }

    @Override
    public TLinkable getPrevious() {
        return prev;
    }

    @Override
    public void setNext(TLinkable tLinkable) {
        next = (Feature)tLinkable;
    }

    @Override
    public void setPrevious(TLinkable tLinkable) {
        prev = (Feature)tLinkable;
    }
}

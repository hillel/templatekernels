package mstparser;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class KernelRerankerTest {

    ParserOptions kernelOptions;
    KernelReranker kernelReranker;

    ParserOptions normalOptions;
    KernelReranker perceptronReranker;


    @Before
    public void setUp() throws Exception {

        String[] kernelArgs = {"train-reranker", "kernel-order:1", "reranker-iters:5",
                               "reranker-model-name:data/test_model.bin",
                               "train-file:data/english_ptb_train.conll.predpos.30best",
                               "validation-file:data/english_ptb_dev.conll.predpos.30best",
                               "use-base-score:false", "loss-type:nopunc" ,
                               "reranker-labeled-loss:false", "base-feats:0", "autofix-feats:0",
                                "max-train-sentences:25", "max-trees-per-sentence:25",
                                "thread-pool-size:8", "use-averaging:true", "lambda:1.0",
                                "reranker-prune-k:30", "format:RERANKER", "reranker-type:kernel"};
        kernelOptions = new ParserOptions(kernelArgs);
        KernelRerankerPipe kernelPipe = new KernelRerankerPipe(kernelOptions);
        kernelReranker = new KernelReranker(kernelPipe, kernelOptions);

        String[] normalArgs = {"train-reranker", "kernel-order:0", "reranker-iters:5",
                "reranker-model-name:data/test_mode.bin",
                "train-file:data/english_ptb_train.conll.predpos.30best",
                "validation-file:data/english_ptb_dev.conll.predpos.30best",
                "use-base-score:false", "loss-type:nopunc" ,
                "reranker-labeled-loss:false", "base-feats:0", "autofix-feats:0",
                "max-train-sentences:25", "max-trees-per-sentence:25", "use-averaging:true", "lambda:1.0",
                "reranker-prune-k:30", "format:RERANKER", "reranker-type:kernel"};
        normalOptions = new ParserOptions(normalArgs);
        KernelRerankerPipe normalPipe = new FirstOrderRerankerPipe(normalOptions);
        perceptronReranker = new KernelReranker(normalPipe, normalOptions);

    }

    @Test
    public void testTrain() throws Exception {
        long start = System.currentTimeMillis();
        kernelReranker.train(kernelOptions.trainfile);
        long end = System.currentTimeMillis();
        System.out.printf("Kernel training took %d seconds%n", (end-start)/1000);
        perceptronReranker.train(normalOptions.trainfile);
        Dataset kernelDevSet = kernelReranker.devset;
        Dataset perceptronDevSet = perceptronReranker.devset;
        assertEquals(kernelDevSet.size(), perceptronDevSet.size());

        for (int i=0; i < kernelDevSet.size(); ++i) {

            kernelReranker.kernelParams.clearKBestCache();

            for (int j=1; j<kernelDevSet.get(i).size(); ++j) {


                perceptronDevSet.addFeaturesToTrees(perceptronDevSet.get(i), perceptronReranker.getPipe());
                kernelDevSet.addFeaturesToTrees(kernelDevSet.get(i), kernelReranker.getPipe());

                assertEquals(kernelReranker.getScore(kernelDevSet, i, j, false), perceptronReranker.getScore(perceptronDevSet, i, j, false), 1e-10);

                perceptronDevSet.removeFeaturesFromTrees(perceptronDevSet.get(i));
                kernelDevSet.removeFeaturesFromTrees(kernelDevSet.get(i));

            }
        }
    }


    @Test
    public void testModelSerailization() throws Exception {
        String[] kernelArgs = {"train-reranker" ,"reranker-iters:5",
                "reranker-model-name:data/test_model.bin",
                "train-file:data/english_ptb_train.conll.predpos.30best",
                "validation-file:data/english_ptb_dev.conll.predpos.30best",
                "use-base-score:false", "loss-type:nopunc" ,
                "reranker-labeled-loss:false", "base-feats:2", "autofix-feats:0",
                "max-train-sentences:25", "max-trees-per-sentence:25",
                "thread-pool-size:8", "use-averaging:false", "lambda:1.0",
                "reranker-prune-k:30", "format:RERANKER", "reranker-type:kernel"};
        kernelOptions = new ParserOptions(kernelArgs);
        KernelRerankerPipe kernelPipe = new KernelRerankerPipe(kernelOptions);
        kernelReranker = new KernelReranker(kernelPipe, kernelOptions);

        long start = System.currentTimeMillis();
        double lossFromTrainProcess = kernelReranker.train(kernelOptions.trainfile);
        long end = System.currentTimeMillis();
        System.out.printf("Kernel training took %d seconds%n", (end-start)/1000);
        System.out.print("Saving model...");
        kernelReranker.saveModel(kernelOptions.rerankerModelName);
        System.out.print("done.");


        String[] testArgs = {"test-kernel-reranker", "reranker-model-name:data/test_model.bin",
                            "train-file:data/english_ptb_train.conll.predpos.30best", "test-file:data/english_ptb_dev.conll.predpos.30best",
                            "thread-pool-size:1", "format:RERANKER", "reranker-type:kernel"};
        kernelOptions = new ParserOptions(testArgs);
        kernelPipe = new KernelRerankerPipe(kernelOptions);
        kernelReranker = new KernelReranker(kernelPipe, kernelOptions);
        kernelReranker.loadModel(kernelOptions.rerankerModelName);
        double lossFromTestCommand = kernelReranker.test();
        assertEquals(lossFromTrainProcess, lossFromTestCommand, 1e-10);


    }
}
package mstparser;

import org.javatuples.Quintet;
import org.javatuples.Triplet;
import org.javatuples.Pair;

import java.io.IOException;

/**
 * This class is used for testing the KernelReranker.
 * The idea is to create a pipe that would be identical to add normal
 * features that are identical to expanded kernel features. We expect the results
 * of a perceptron based on this pipe to be identical to the results of a kernel
 * perceptron.
 */
public class FirstOrderRerankerPipe extends KernelRerankerPipe {


    Alphabet dataAlphabet = new Alphabet();

    public FirstOrderRerankerPipe(ParserOptions options) throws IOException, ClassNotFoundException {
        super(options);
    }

    // add with default 1.0
    public void addPairFeature(Pair<Long, Long> feat, FeatureVector fv) {

        int num = dataAlphabet.lookupIndex(feat);
        //System.out.println(String.format("Adding feature %s with index %d", feat, num));
        if(num >= 0)
            fv.add(num, 1.0);
    }

    // add with default 1.0
    public void addTripletFeature(Triplet<Long, Long, Long> feat, FeatureVector fv) {

        int num = dataAlphabet.lookupIndex(feat);
        //System.out.println(String.format("Adding feature %s with index %d", feat, num));
        if(num >= 0)
            fv.add(num, 1.0);
    }

    // add with default 1.0
    public void addQuintetFeature(Quintet<Long, Long, Long, Long, Long> feat, FeatureVector fv) {

        int num = dataAlphabet.lookupIndex(feat);
        //System.out.println(String.format("Adding feature %s with index %d", feat, num));
        if(num >= 0)
            fv.add(num, 1.0);
    }



    @Override
    public int getDataAlphabetSize() {
        return dataAlphabet.size();
    }

    @Override
    public void closeAlphabets() {
        dataAlphabet.stopGrowth();
        typeAlphabet.stopGrowth();
    }


    public FeatureVector createFeatureVector(DependencyInstance instance)  {
        FeatureVector fv = new FeatureVector();
        for (int i=0; i<instance.length(); ++i){
            if (instance.heads[i] == -1) {
                continue;
            }
            int headIdx = instance.heads[i];
            int depIdx = i;
            long[] headFeats = instance.nodeFeats[headIdx];
            long[] edgeFeats = instance.edgeFeats[depIdx];
            long[] depFeats = instance.nodeFeats[depIdx];
            for (int j=0; j<headFeats.length; ++j) {
                for (int l=0; l<depFeats.length; ++l) {
                    Pair<Long, Long> pairFeat = Pair.with(headFeats[j], depFeats[l]);
                    addPairFeature(pairFeat, fv);
                    for (int k=0; k<edgeFeats.length; ++k) {
                        Triplet<Long, Long, Long> tripletFeat = Triplet.with(headFeats[j], edgeFeats[k], depFeats[l]);
                        addTripletFeature(tripletFeat, fv);
                    }
                }
            }
            /*
            // If the modifier has a right sibling, add a quintet feature for them.
            int sibIdx = -1;
            for (int k=depIdx+1; k<instance.length(); ++k) {
                if (instance.heads[k] == headIdx) {
                    sibIdx = k;
                    break;
                }
            }
            if (sibIdx > -1) {
                long[] edge2Feats = instance.edgeFeats[sibIdx];
                long[] sibFeats = instance.nodeFeats[sibIdx];
                for (int j=0; j<headFeats.length; ++j) {
                    for (int k=0; k<edgeFeats.length; ++k) {
                        for (int l=0; l<depFeats.length; ++l) {
                            for (int m=0; m<edge2Feats.length; ++m) {
                                for (int n=0; n<sibFeats.length; ++n) {
                                    Quintet<Long, Long, Long, Long, Long> feat = Quintet.with(headFeats[j],
                                                                                  edgeFeats[k],
                                                                                  depFeats[l],
                                                                                  edge2Feats[m],
                                                                                  sibFeats[n]);
                                    addQuintetFeature(feat, fv);
                                }
                            }

                        }
                    }
                }
            }
            */

        }

        return fv;
    }


}

package mstparser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: hillelt
 * Date: 11/1/13
 * Time: 5:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class DependencyInstanceTest {

    DependencyInstance instance;

    @Before
    public void setUp() throws Exception {

        String[] forms = new String[] {"<root>", "John", "ate", "raisins"};
        String[] postags = new String[] {"<root-POS>", "NNP", "VBD", "NNS"};
        String[] labs = new String[] {"<no-type>", "subj", "root", "dobj"};
        int[] heads = new int[] {-1, 2, 0, 2};
        double score = 177.2;
        instance = new DependencyInstance(forms, postags, labs,  heads, score);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testInitializeDeps() throws Exception {
        instance.initializeDeps();

        assertTrue(instance.deps[0].length==1 && instance.deps[0][0]==2);
        assertTrue(instance.deps[1].length==0);
        assertTrue(instance.deps[2].length==2 && instance.deps[2][0]==1 && instance.deps[2][1]==3);
        assertTrue(instance.deps[3].length==0);
    }
}
